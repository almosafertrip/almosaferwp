﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using almosaferwp;
using System.Net.Mail;
using System.Net;

namespace AlmosaferWP
{
    public partial class contact_us : basePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    myAlert.Visible = false;
                    addPlaceHolders();
                }
            }
            catch (Exception)
            {

                myAlert.Visible = true;
            }

        }

        protected void btnSendMsg_Click(object sender, EventArgs e)
        {
            try
            {
                // SendMail();
                
            }
            catch 
            {
                myAlert.Visible = true;
               
            }
            
        }
        protected void SendMail()
        {

            // start the list trial
            //Mail notification
            MailMessage message = new MailMessage();
            message.Subject = "Email from our milestone ";
            string subject = textSubject.Text;
            string phone =  textPhone.Text;
            string body = "From: " + textName.Text + "\n";
            body += "Email: " + textMail.Text + "\n";
            body += "Subject: " + subject + "\n";
            body += "Phone: " + phone + "\n";
            body += "Quick Description: \n" + textMsg.Text + "\n";
            message.Body = body;
            string mailFrom = textMail.Text;
            message.From = new MailAddress(mailFrom);
            // Email Address from where you send the mail
            //var fromAddress = "databasemil3@gmail.com";
            var fromAddress = "anymail@gmail.com";
            //message.To = 


            //Password of your mail address
            //const string fromPassword = "D@milestone3";
            const string fromPassword = "anypass";

            // START list trial
            // this is to send emails to multiple recipients
            // *
            // * 
            // *
            // smtp settings
            var smtp = new System.Net.Mail.SmtpClient();
            {
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                smtp.Timeout = 20000;
            }

            // Passing values to smtp object        
            smtp.Send(textMail.Text, fromAddress, subject, body);

            // Call Close when done reading.
        }
        protected void addPlaceHolders()
        {

            textName.Attributes.Add("placeholder", Resources.General.aspx.lblname1.ToString());
            textMail.Attributes.Add("placeholder", Resources.General.aspx.lblEmail1.ToString());
            textSubject.Attributes.Add("placeholder", Resources.General.aspx.lblsubje1.ToString());
            textPhone.Attributes.Add("placeholder", Resources.General.aspx.lblphon1.ToString());
            textMsg.Attributes.Add("placeholder", Resources.General.aspx.lblmsg1.ToString());

        }
    }
}