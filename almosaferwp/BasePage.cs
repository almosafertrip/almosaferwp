﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Text.RegularExpressions;
using System.Globalization;

namespace almosaferwp
{
    public class basePage : System.Web.UI.Page
    {

        protected override void InitializeCulture()
        {
            if (Session["SelectedLanguage"] == null)
            {
                Session["SelectedLanguage"] = "English";
            }
            string lang = Session["SelectedLanguage"].ToString();
            string str;
            if (lang == "Arabic")
            {
                str = "ar-EG";
            }
            else
            {
                str = "en";
            }

            System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(str);
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(str);
            //base.InitializeCulture();

        }
    }
}