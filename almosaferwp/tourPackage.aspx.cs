﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net;

using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Web.UI.HtmlControls;

namespace almosaferwp
{
    public partial class tourPackage : basePage
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    loadPackage();
                    loadPackageRec();
                    myAlert.Visible = false;

                }
            }
            catch
            {
                myAlert.Visible = true;
            }
        }
        protected void loadPackage()
        {
            string tblName = "tourPackage";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "tourPackageAr";
            }
            DataSet dsPack = codeClass.Sqlread("select * from "+ tblName +" where ID = " + Request.QueryString["ID"].ToString());
            lblTitle.Text = dsPack.Tables[0].Rows[0]["name"].ToString();
            lvTourPack.DataSource = dsPack;
            lvTourPack.DataBind();
        }
        protected void loadPackageRec()
        {
            string tblName = "Pakageelmosafer";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "PakageelmosaferAr";
            }
            DataSet dsPackrec = codeClass.Sqlread("select top (6) * from "+ tblName +" where tourID <> " + Request.QueryString["ID"].ToString());
            //lblTitle.Text = dsPackrec.Tables[0].Rows[0]["name"].ToString();
            lvPackRec.DataSource = dsPackrec;
            lvPackRec.DataBind();
        }

        protected void lvTourPack_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                loadTourItin();
                loadTourGuid();
                loadHotelInfo();
                loadChoosePack();
                loadHelpData();
            }
            catch
            {
                //myAlert.Visible = true;
            }
        }
        protected void loadHelpData()
        {
            string tblName = "ContactUS";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "ContactUSAr";
            }
            ListView lvCont = (ListView)lvTourPack;
            ListViewDataItem lvContItem = (ListViewDataItem)lvCont.FindControl("ctrl0");
            Label lblPhoneHelp = (Label)lvContItem.FindControl("lblPhoneHelp");
            Label lblEmailHelp = (Label)lvContItem.FindControl("lblEmailHelp");
            DataSet dsTHelp = codeClass.Sqlread("select top (1) * from " + tblName );
            lblPhoneHelp.Text = dsTHelp.Tables[0].Rows[0]["number"].ToString();
            lblEmailHelp.Text = dsTHelp.Tables[0].Rows[0]["Emailtype"].ToString();


        }
        protected void loadTourItin()
        {
            string tblName = "tourItinerary";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "tourItineraryAr";
            }
            ListView lvCont = (ListView)lvTourPack;
            ListViewDataItem lvContItem = (ListViewDataItem)lvCont.FindControl("ctrl0");
            ListView lvTourItin = (ListView)lvContItem.FindControl("lvTourItin");
            DataSet dsTourInit = codeClass.Sqlread("select * from "+ tblName +" where tourID = " + Request.QueryString["ID"].ToString());
            lvTourItin.DataSource = dsTourInit;
            lvTourItin.DataBind();
        }

        protected void loadTourGuid()
        {
            string tblName = "tourPackage";
            string tblGuide = "tourGuid"; 
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "tourPackageAr";
                tblGuide = "tourGuidAr";

            }
            ListView lvCont = (ListView)lvTourPack;
            ListViewDataItem lvContItem = (ListViewDataItem)lvCont.FindControl("ctrl0");
            ListView lvTourGuid = (ListView)lvContItem.FindControl("lvTourGuid");
            DataSet dsPack = codeClass.Sqlread("select * from "+ tblName +" where ID = " + Request.QueryString["ID"].ToString());
            int GuidID = int.Parse(dsPack.Tables[0].Rows[0]["guidID"].ToString());
            DataSet dsTourGuid = codeClass.Sqlread("select * from "+ tblGuide +" where ID = " + GuidID);
            lvTourGuid.DataSource = dsTourGuid;
            lvTourGuid.DataBind();
        }
        protected void loadHotelInfo()
        {
            string tblName = "tourPackage";
            string tblHotel = "tourHotel";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "tourPackageAr";
                tblHotel = "tourHotelAr";

            }
            ListView lvCont = (ListView)lvTourPack;
            ListViewDataItem lvContItem = (ListViewDataItem)lvCont.FindControl("ctrl0");
            ListView lvHotelInfo = (ListView)lvContItem.FindControl("lvHotelInfo");
            DataSet dsPack = codeClass.Sqlread("select * from " + tblName + " where ID = " + Request.QueryString["ID"].ToString());
            int HotelID = int.Parse(dsPack.Tables[0].Rows[0]["hotelID"].ToString());
            DataSet dsTourHotel = codeClass.Sqlread("select * from " + tblHotel + " where ID = " + HotelID);
            lvHotelInfo.DataSource = dsTourHotel;
            lvHotelInfo.DataBind();
        }

        protected void loadChoosePack()
        {
            string tblChoosePack = "choosePack";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblChoosePack = "choosePackAr";

            }
            ListView lvCont = (ListView)lvTourPack;
            ListViewDataItem lvContItem = (ListViewDataItem)lvCont.FindControl("ctrl0");
            ListView lvChoosePack = (ListView)lvContItem.FindControl("lvChoosePack");
            DataSet dsChoosePack = codeClass.Sqlread("select * from " + tblChoosePack + " where tourID = " + Request.QueryString["ID"].ToString());
            lvChoosePack.DataSource = dsChoosePack;
            lvChoosePack.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //string subject = "Email from our milestone ";
            //string mailFrom = txtEmail.Text;
            //string body = "From: " + txtName.Text + "\n";
            //body += "Email: " + txtEmail.Text + "\n";
            //body += "Subject: " + subject + "\n";
            //body += "Quick Description: \n" + txtDescription.Text + "\n";
            //string mailTo = "anymail@gmail.com";

            // SendMail();
        }

        protected void SendMail()
        {

            // start the list trial
            //Mail notification
            MailMessage message = new MailMessage();
            message.Subject = "Email from our milestone ";
            string subject = "Email from our milestone ";
            string body = "From: " + txtName.Text + "\n";
            body += "Email: " + txtEmail.Text + "\n";
            body += "Subject: " + subject + "\n";
            body += "Quick Description: \n" + txtDescription.Text + "\n";
            message.Body = body;
            string mailFrom = txtEmail.Text;
            message.From = new MailAddress(mailFrom);
            // Email Address from where you send the mail
            //var fromAddress = "databasemil3@gmail.com";
            var fromAddress = "anymail@gmail.com";
            //message.To = 


            //Password of your mail address
            //const string fromPassword = "D@milestone3";
            const string fromPassword = "anypass";

            // START list trial
            // this is to send emails to multiple recipients
            // *
            // * 
            // *
            // smtp settings
            var smtp = new System.Net.Mail.SmtpClient();
            {
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                smtp.Timeout = 20000;
            }
            
                // Passing values to smtp object        
                smtp.Send(txtEmail.Text, fromAddress,subject, body);

                // Call Close when done reading.
            }
        // end the list trial

        protected void btnBookModal_Click(object sender, EventArgs e)
        {
            //Panel1.Visible = true;
            return;
        }
    }
}
