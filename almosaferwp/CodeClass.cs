﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Text.RegularExpressions;

namespace almosaferwp
{
    public class CodeClass
    {
        //Data Access Layer
        public DataSet Sqlread(string sqlquery)
        {
            DataSet functionReturnValue = null;
            try
            {
                SqlConnection thisConnection = null;
                thisConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Connstr"].ToString());
                string sql = sqlquery;

                thisConnection.Open();
                DataSet DS = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(sql, thisConnection);
                da.Fill(DS);
                functionReturnValue = DS;
                thisConnection.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return functionReturnValue;
        }
        public string Sqlinsert(string sqlquery)
        {
            string functionReturnValue = null;
            try
            {
                SqlConnection thisConnection = null;
                thisConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["Connstr"].ToString());
                string sql = sqlquery;

                thisConnection.Open();

                dynamic objCmd = null;
                objCmd = new System.Data.SqlClient.SqlCommand();
                var _with1 = objCmd;
                _with1.Connection = thisConnection;
                _with1.CommandType = CommandType.Text;
                _with1.CommandText = sql;
                objCmd.ExecuteNonQuery();




                functionReturnValue = "Data insertion Successful !";
                thisConnection.Close();

            }
            catch (Exception ex)
            {
                //  Lookups.LogErrorToText("Web", "Settings.vb", "FillMenus", ex.Message)

            }
            return functionReturnValue;
        }
        public string Get_RCMS_XML(string ID)
        {
            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = Sqlread("SELECT * FROM RCMS_XML WHERE ID=" + ID + "").Tables[0];
                using (StreamWriter sw = File.CreateText(AppDomain.CurrentDomain.BaseDirectory + "/XML/" + dt.Rows[0]["ID"] + ".xml"))
                {
                    sw.Write(Convert.ToString(dt.Rows[0]["XML"]));

                }

                return dt.Rows[0]["ID"] + ".XML";

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}