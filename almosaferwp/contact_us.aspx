﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="contact_us.aspx.cs" Inherits="AlmosaferWP.contact_us" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ MasterType VirtualPath="~/Master.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal ID="lblTitle" Text="<%$Resources:General.aspx,lblTitle%>" meta:resourcekey="lblTitle" runat="server" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        #main {
            background-color: white !important;
            /*fake*/
        }
                /* alert style */
        #CPContent_myAlert {
        
            width: 80%;
            margin: auto;
            position: fixed;
            left: 10.5%;
            top: 20%;
            height: auto;
            word-break: break-word;
            z-index: 4444;

        }
        #CPContent_myAlert div {
            
            height: inherit;
            font-size: large;
            text-align: center;
            border: black 1px solid;

        }
        .bg-contact, .contact-submit {
            margin-bottom : 20px;
        }
        .input-group-addon {
            border:none;
        }
        .subtitle-cover.sub-title {
    margin-bottom: 20px !important;
}
    </style>
    <%-- mas --%>
<%--    <script type="text/javascript" src="http://maps.google.com.mx/maps/api/js?sensor=true&language=es"></script>
    <script src='http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js'></script>
    <script type="text/javascript">
        function initialize() {
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var myOptions = {
                zoom: 8,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        }
        $(document).load(initialize());

    </script>--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <section id="main">

        <div class="subtitle-cover sub-title " style="background-image: url(../wp-content/uploads/2016/10/contact-page.jpg); background-size: cover; background-position: 50% 50%;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-leading">
                            <asp:Literal ID="Literal1" Text="<%$Resources:General.aspx,lblTitle%>" meta:resourcekey="lblTitle" runat="server" /></h2>
                        <ol class="breadcrumb">
                            <li><a href="default.aspx" class="breadcrumb_home">
                                <asp:Literal ID="Literal3" Text="<%$Resources:General.aspx,lblHome%>" meta:resourcekey="lblHome" runat="server" /></a></li>
                            <li class="active">
                                <asp:Literal ID="Literal2" Text="<%$Resources:General.aspx,lblTitle%>" meta:resourcekey="lblTitle" runat="server" /></li>
                        </ol>

                    </div>
                </div>
            </div>
        </div>
        <!--/.sub-title-->

        <div class="container">
            <div id="content" class="site-content" role="main">


                <div id="post-979" class="post-979 page type-page status-publish hentry">


                    <div class="entry-content">
                        <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1476443477559 vc_row-no-padding" style="position: relative; left: -206.5px; box-sizing: border-box; width: 1583px;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="wpb_gmaps_widget wpb_content_element vc_custom_1516994815457 wpb_animate_when_almost_visible wpb_bounceIn bounceIn wpb_start_animation animated">
		<div class="wpb_wrapper">
		<div class="wpb_map_wraper">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3453.927221889351!2d31.20195771535827!3d30.03894572578687!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145846cd9fc8ad59%3A0xa75664bcd9f3c1ce!2s56+Mossadak%2C+Ad+Doqi%2C+Giza+Governorate!5e0!3m2!1sen!2seg!4v1516994783017" width="600" height="650" frameborder="0" style="border: 0px; pointer-events: none;" allowfullscreen=""></iframe>		</div>
	</div>
</div>
</div></div></div></div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1476444361716">
                            <div class="wpb_column vc_column_container vc_col-sm-5">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="addon-themeum-action text-uppercase" style="text-align: left;">
                                            <div class="themeum-action">
                                                <h2 class="action-titlecustomstyle" style="font-size: 20px; line-height: normal; color: #000000; margin: 0px 0 2px 0;">
                                                    <asp:Label runat="server" ID="lblOffi" Text="<%$Resources:tourPackage.aspx,lblOffi%>" meta:resourcekey="lblOffi"></asp:Label>
                                                    </h2>
                                            </div>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element  vc_custom_1516993900158">
                                            <div class="wpb_wrapper">
                                                <p>
                                                    <strong>
                                                        
                                                        <asp:Label runat="server" ID="lblADDR" Text="<%$Resources:tourPackage.aspx,lblADDR%>" meta:resourcekey="lblADDR"></asp:Label>
                                                    </strong><br />
                                                    <asp:Label runat="server" ID="lblADDR2" Text="<%$Resources:tourPackage.aspx,lblADDR2%>" meta:resourcekey="lblADDR2"></asp:Label>
                                                </p>
                                                <p>
                                                    <b>
                                                        <asp:Label runat="server" ID="lblweb" Text="<%$Resources:tourPackage.aspx,lblweb%>" meta:resourcekey="lblweb"></asp:Label>
                                                </p>
                                                <p>
                                                </p>
                                                <p>
                                                    <strong>
                                                </p>
                                                <p>
                                                    <asp:Literal Text="info@almosafertrip.net" runat="server" />
                                                   
                                                </p>
                                                <p>
                                                    </strong>
                                                     <asp:Literal Text="+01234 5678910" runat="server" />
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-7">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="addon-themeum-action text-uppercase" style="text-align: left;">
                                            <div class="themeum-action">
                                                <h2 class="action-titlecustomstyle" style="font-size: 20px; line-height: normal; color: #000000; margin: 0px 0 35px 0;">
                                                    <asp:Label runat="server" ID="lblcontc" Text="<%$Resources:tourPackage.aspx,lblcontc%>" meta:resourcekey="lblcontc"></asp:Label>
                                                    </h2>
                                            </div>
                                        </div>
                                        <div role="form" class="wpcf7" id="wpcf7-f978-p979-o1" lang="en-US" dir="ltr">
                                            <div class="screen-reader-response"></div>
                                            <asp:Panel runat="server" class="wpcf7-form" >
                                                <div style="display: none;">
                                                    <input type="hidden" name="_wpcf7" value="978" />
                                                    <input type="hidden" name="_wpcf7_version" value="4.9.2" />
                                                    <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f978-p979-o1" />
                                                    <input type="hidden" name="_wpcf7_container_post" value="979" />
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="bg-contact  input-group">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-user"></span>
                                                                </span>
                                                                <asp:TextBox runat="server" ID="textName" CssClass="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" ></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="bg-contact  input-group">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-envelope"></span>
                                                                </span>
                                                                <asp:TextBox runat="server" ID="textMail" TextMode="Email" CssClass="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" ></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="bg-contact  input-group">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-tags"></span>
                                                                </span>
                                                                <asp:TextBox runat="server" ID="textSubject" CssClass="wpcf7-form-control wpcf7-text" ></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="bg-contact  input-group">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-phone"></span>
                                                                </span>
                                                                <asp:TextBox runat="server" ID="textPhone" CssClass="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" ></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="bg-contact  input-group">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-text-height"></span>
                                                                </span>
                                                                <asp:TextBox runat="server" ID="textMsg" TextMode="MultiLine" CssClass="wpcf7-form-control wpcf7-textarea" Columns="40" Rows="10"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="contact-submit">
                                                            <%--<input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit" />--%>
                                                            <asp:Button runat="server" ID="btnSendMsg" OnClick="btnSendMsg_Click" CssClass="wpcf7-form-control wpcf7-submit" Text="<%$Resources:General.aspx,lblContactMsg%>" meta:resourcekey="lblContactMsg" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/#content-->
        
            </div>

    </section>


    <%--Start Alert--%>
    <asp:Panel ID="myAlert" runat="server">
        <h2 class="text-center"><a href="default.aspx"><asp:Literal ID="Literal4" Text="<%$Resources:General.aspx,lblHomePg%>" meta:resourcekey="lblHomePg" runat="server" /></a></h2>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> <asp:Label ID="lblAlert"  runat="server" Text="<%$Resources:Default.aspx,lblAlert%>" meta:resourcekey="lblAlert" />!</strong>
        </div>
        <div class="modalBG alert-dismissible" style="
            min-height: 1602px;
            position: fixed;
            width: 100%;
            top: 0;
            left: 0;
            background: rgba(0,0,0,0.8);
            z-index: -1;
        "></div>
    </asp:Panel>
    <%--End Alert--%>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
