﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Default.aspx.cs" Inherits="AlmosaferWP.Default" meta:resourcekey="PageResource1"  Culture="auto:ar-eg" UICulture="auto" %>
<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="System.Globalization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    Travel Easy!
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <%--<link href="wp-content/themes/travelkit/css/maindc98.css" rel="stylesheet" />--%>
    <%--<script>
            //$(".thm-tk-tab").click(function () {
            $('.thm-tk-tab').on('click', 'div', function (e) {
                alert("clicked");
                e.preventDefault();
                var tab = $(this);
                alert(tab);
            //var newString = $(location).attr('pathname');
            //var url = newString.replace('/', '');
            //alert(url);
                if ($(".thm-tk-tab").hasClass('active'))
            {
                    $(".thm-tk-tab").removeClass('active');
            }
                alert("active removed");
            tab.addClass('active');
        });
    </script>--%>
    <style>
        .thm-tk-tab {
            display: none !important;
        }

            .thm-tk-tab.active {
                display: block !important;
            }
    </style>
    <style>
        /* xs */
        @media (max-width: 767.98px) {
            .thm-tk-search-btn {
                min-width: 11%;
                margin-left: unset;
            }
        }
        /* sm */
        @media (min-width: 768px) and (max-width: 991.98px) {
            .thm-tk-search-btn {
                min-width: 11%;
                margin-left: unset;
            }
        }
        /* md */
        @media (min-width: 992px) and (max-width: 1199.98px) {
            .thm-tk-search-btn {
                min-width: 11%;
            }
        }
        /* lg */
        @media (min-width: 1200px) {
            .thm-tk-search-btn {
                min-width: 9.2%;
            }
        }
        /* hide form */
        .thm-tk-tab-inner form {
            display:none !important;
        }

        /* alert style */
        #CPContent_myAlert {
        
            width: 80%;
            margin: auto;
            position: fixed;
            left: 10.5%;
            top: 20%;
            height: auto;
            word-break: break-word;
            z-index: 4444;

        }
        #CPContent_myAlert div {
            
            height: inherit;
            font-size: large;
            text-align: center;
            border: black 1px solid;

        }
        /* btn email subscribe*/
        .thm-tk-search-btn {
            margin-top: unset;
            height: 50px;
            margin-left: -4.4%; 
            min-width: 20.6%;
        }
        .thm-tk-search-btn span {
            font-size:16px;
        }
    </style>


    
        

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <section id="main" class="clearfix">
        <div class="container">
            <div id="content" class="site-content" role="main">

                <div id="post-11" class="post-11 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">

                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <link href="http://fonts.googleapis.com/css?family=Montserrat:400%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all">
                                                <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin: 0px auto; background: transparent; padding: 0px; margin-top: 0px; margin-bottom: 0px;">
                                                    <!-- START REVOLUTION SLIDER 5.4.6.3.1 fullwidth mode -->
                                                    <section class="carousel slide cid-qWJRtMJ2Rr" data-interval="false" id="slider1-3">
                                                        <div class="full-screen">
                                                            <%-- slider show  --%>
                                                            <div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="false" data-interval="3000">
                                                                <div class="carousel-inner" role="listbox">
                                                                    <%-- first pic --%>
                                                                    <asp:ListView ID="lvSlider" ClientIDMode="Static" runat="server">
                                                                        <ItemTemplate>
                                                                            <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(<%# Eval("url") %>);">
                                                                                <div class="container container-slide">
                                                                                    <div class="image_wrapper">
                                                                                        <div class="mbr-overlay"></div>
                                                                                        <img src="<%# Eval("url") %>"><div class="carousel-caption justify-content-center">
                                                                                            <div class="col-10 align-center">
                                                                                                <h2 class="mbr-fonts-style display-1"><%# Eval("name") %></h2>
                                                                                                <p class="lead mbr-text mbr-fonts-style display-5" dir="rtl"><%# Eval("textDesc") %></p>
                                                                                                <div class="mbr-section-btn" buttons="0"><a class="btn btn-success display-4" href="<%# Eval("urlRedirect") %>">
                                                                                                    <asp:label ID="lblstrtour"  runat="server" Text="<%$Resources:Default.aspx,lblstrtour%>" meta:resourcekey="lblstrtour">START TOUR </asp:label>
                                                                                                    </a> </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:ListView>
<%--                                                                    <%--second pic  
                                                                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(assets/images/2.jpg);">
                                                                        <div class="mbr-overlay"></div>
                                                                        <div class="container container-slide">
                                                                            <div class="image_wrapper">
                                                                                <img src="assets/images/2.jpg" style="opacity: 0;"><div class="carousel-caption justify-content-center">
                                                                                    <div class="col-10  text-align : center-block">
                                                                                        <h2 class="mbr-fonts-style display-1">Seasons America</h2>
                                                                                        <p class="lead mbr-text mbr-fonts-style display-5">START FROM $599</p>
                                                                                        <div class="mbr-section-btn" buttons="0"><a class="btn btn-info display-4" href="https://mobirise.com">START TOUR</a> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                     Third pic 
                                                                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(assets/images/4.jpg);">
                                                                        <div class="mbr-overlay"></div>
                                                                        <div class="container container-slide">
                                                                            <div class="image_wrapper">
                                                                                <img src="assets/images/2.jpg" style="opacity: 0;"><div class="carousel-caption justify-content-center">
                                                                                    <div class="col-10  text-align : center-block  vc_color-white">
                                                                                        <h2 class="mbr-fonts-style display-1">Dubai</h2>
                                                                                        <p class="lead mbr-text mbr-fonts-style display-5">START FROM $299</p>
                                                                                        <div class="mbr-section-btn" buttons="0"><a class="btn btn-info display-4" href="https://mobirise.com">START TOUR</a> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                     Fourth pic 
                                                                    <div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(assets/images/3.jpg);">
                                                                        <div class="container container-slide">
                                                                            <div class="image_wrapper">
                                                                                <div class="mbr-overlay"></div>
                                                                                <img src="assets/images/3.jpg"><div class="carousel-caption justify-content-center">
                                                                                    <div class="col-10 text-align : center-block">
                                                                                        <h2 class="mbr-fonts-style display-1">Arakaki Realtor</h2>
                                                                                        <p class="lead mbr-text mbr-fonts-style display-5">START FROM $499</p>
                                                                                        <div class="mbr-section-btn" buttons="0"><a class="btn btn-info display-4" href="https://mobirise.com">START TOUR</a> </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>--%>
                                                                </div>
                                                                <a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#slider1-3"><span aria-hidden="true" class="mbri-left mbr-iconfont"></span><span class="sr-only">Previous</span></a><a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next" href="#slider1-3"><span aria-hidden="true" class="mbri-right mbr-iconfont"></span><span class="sr-only">Next</span></a>
                                                            </div>
                                                        </div>
                                                        <div data-vc-full-width="true" data-vc-full-width-init="true" class="vc_row wpb_row vc_row-fluid show-overflow package-search-wrapper vc_custom_1505974672499 vc_row-has-fill" style="position: relative; display: none !important; left: -189.6px; box-sizing: border-box; width: 1519px; padding-left: 189.6px; padding-right: 189.4px;">
                                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                <div class="vc_column-inner ">
                                                                    <div class="wpb_wrapper">
                                                                        <div class="thm-tk-search">
                                                                            <div class="thm-tk-search-nav">
                                                                                <ul>
                                                                                    <li><a href="#holidays" class="active"><i class="icon-location"></i><asp:Label ID="lblHoliday" runat="server" Text="<%$Resources:Default.aspx,lblHoliday%>" meta:resourcekey="lblHoliday">Holidays</asp:Label></a></li>
                                                                                    <li><a href="#hotel" class=""><i class="icon-hotel"></i><span>Hotels &amp; Resorts</span></a></li>
                                                                                    <li><a href="#flights"><i class="icon-flight"></i><span>Flights</span></a></li>
                                                                                    <li><a href="#vehicles" class=""><i class="icon-vehicles"></i><span>Vehicles</span></a></li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="thm-tk-search-form">
                                                                                <div class="thm-tk-tab active" id="holidays">
                                                                                    <div class="thm-tk-tab-inner clearfix">
                                                                                        <form id="thm-tk-advancedsearch-form" class="clearfix" action="http://almosafertrip.net/" method="GET">
                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label><asp:Label ID="lblSearchHol"  Text="<%$Resources:Default.aspx,lblSearchHol%>" meta:resourcekey="lblSearchHol" runat="server" /></label>
                                                                                                <input type="text" name="s" class="thm-tk-input-first" placeholder="Type keyword" value="">
                                                                                            </div>

                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label><asp:Label ID="lblLocHol1"  Text="<%$Resources:Default.aspx,lblLocHol%>" meta:resourcekey="lblLocHol" runat="server" /></label>
                                                                                                <select name="location" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                                                    <option value="">Select a Location</option>
                                                                                                    <option value="america">America</option>
                                                                                                    <option value="australia">Australia</option>
                                                                                                    <option value="bangladesh">Bangladesh</option>
                                                                                                    <option value="dhaka">Dhaka</option>
                                                                                                    <option value="france">France</option>
                                                                                                    <option value="india">India</option>
                                                                                                    <option value="london">London</option>
                                                                                                    <option value="russia">Russia</option>
                                                                                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 173px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-location-nj-container"><span class="select2-selection__rendered" id="select2-location-nj-container" title="Select a Location">Select a Location</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                            </div>
                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label><asp:Label ID="lblChkInHol"  Text="<%$Resources:Default.aspx,lblChkInHol%>" meta:resourcekey="lblChkInHol" runat="server" /></label>
                                                                                                <input type="text" name="checkin" class="thm-date-picker hasDatepicker" value="" placeholder="Check-in date" id="dp1530620289864">
                                                                                            </div>
                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label>
                                                                                                    <asp:Label ID="lblGuestHol" Text="<%$Resources:Default.aspx,lblGuestHol%>" meta:resourcekey="lblGuestHol" runat="server" /></label>
                                                                                                <input type="number" name="guest" class="" value="" placeholder="Number of guests">
                                                                                            </div>
                                                                                            <input type="hidden" name="post_type" value="package">
                                                                                            <button class="btn btn-primary thm-tk-search-btn" type="submit"><asp:Label ID="Label22" Text="<%$Resources:Default.aspx,lblSearch%>" meta:resourcekey="lblSearch" runat="server" /></button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="thm-tk-tab " id="hotel">
                                                                                    <div class="thm-tk-tab-inner">
                                                                                        <form id="thm-tk-advancedsearch-form2" class="clearfix" action="http://almosafertrip.net/" method="GET">
                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label><asp:Label ID="lblSearchHot" Text="<%$Resources:Default.aspx,lblSearchHot%>" meta:resourcekey="lblSearchHot" runat="server" /></label>
                                                                                                <input type="text" name="s" id="search-keyword" class="thm-tk-input-first" placeholder="Type keyword" value="">
                                                                                            </div>

                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label><asp:Label ID="Label6" Text="<%$Resources:Default.aspx,lblLocationHot%>" meta:resourcekey="lblLocationHot" runat="server" /></label>
                                                                                                <select name="hotel_location" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                                                    <option value="">Select a location</option>
                                                                                                    <option value="abu-dhabi">Abu Dhabi</option>
                                                                                                    <option value="anguilla">Anguilla</option>
                                                                                                    <option value="bahamas">Bahamas</option>
                                                                                                    <option value="dhaka">dhaka</option>
                                                                                                    <option value="dubai">Dubai</option>
                                                                                                    <option value="german">German</option>
                                                                                                    <option value="las-vegas">Las Vegas</option>
                                                                                                    <option value="london">London</option>
                                                                                                    <option value="los-cabos">Los Cabos</option>
                                                                                                    <option value="new-york-city">New York City</option>
                                                                                                    <option value="paris">Paris</option>
                                                                                                    <option value="rome">Rome</option>
                                                                                                    <option value="trivago">Trivago</option>
                                                                                                    <option value="turkey">Turkey</option>
                                                                                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-hotel_location-qa-container"><span class="select2-selection__rendered" id="select2-hotel_location-qa-container" title="Select a location">Select a location</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                            </div>
                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label><asp:Label ID="lblTypeHot" Text="<%$Resources:Default.aspx,lblTypeHot%>" meta:resourcekey="lblTypeHot" runat="server" /></label>
                                                                                                <select name="hotel_category" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                                                    <option value="">Hotel Type</option>
                                                                                                    <option value="3-star">3 Star</option>
                                                                                                    <option value="5-star">5 Star</option>
                                                                                                    <option value="hotel">Hotel</option>
                                                                                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-hotel_category-v7-container"><span class="select2-selection__rendered" id="select2-hotel_category-v7-container" title="Hotel Type">Hotel Type</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                            </div>
                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label>Room Type</label>
                                                                                                <select name="room_type" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                                                    <option value="">Room Type</option>
                                                                                                    <option value="doubleroom">Double Room</option>
                                                                                                    <option value="singleroom">Single Room</option>
                                                                                                    <option value="luxuryroom">Luxury Room</option>
                                                                                                    <option value="generalroom">General Room</option>
                                                                                                    <option value="familyroom">Family Room</option>
                                                                                                    <option value="deluxeroom">Deluxe Room</option>
                                                                                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-room_type-0c-container"><span class="select2-selection__rendered" id="select2-room_type-0c-container" title="Room Type">Room Type</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                            </div>
                                                                                            <input type="hidden" name="post_type" value="hotel">
                                                                                            <button class="btn btn-primary thm-tk-search-btn" type="submit"><asp:Label ID="Label32" Text="<%$Resources:Default.aspx,lblSearch%>" meta:resourcekey="lblSearch" runat="server" /></button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="thm-tk-tab" id="flights">
                                                                                    <div class="thm-tk-tab-inner">
                                                                                        <form id="thm-tk-flights-search-form" class="clearfix" data-tp="0">
                                                                                            <div class="thm-tk-input-5-1 thm-tk-first-select">
                                                                                                <label><asp:Label ID="Label8" Text="<%$Resources:Default.aspx,lblFromFlight%>" meta:resourcekey="lblFromFlight" runat="server" /></label>
                                                                                                <select name="originplace" class="thm-fs-place select2-hidden-accessible" data-placeholder="Origin city or airport" tabindex="-1" aria-hidden="true"></select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-originplace-rf-container"><span class="select2-selection__rendered" id="select2-originplace-rf-container"><span class="select2-selection__placeholder">Origin city or airport</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                            </div>
                                                                                            <div class="thm-tk-input-5-1">
                                                                                                <label><asp:Label ID="Label9" Text="<%$Resources:Default.aspx,lblToFlight%>" meta:resourcekey="lblToFlight" runat="server" /></label>
                                                                                                <select name="destinationplace" class="thm-fs-place select2-hidden-accessible" data-placeholder="Destination city or airport" tabindex="-1" aria-hidden="true"></select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-destinationplace-xp-container"><span class="select2-selection__rendered" id="select2-destinationplace-xp-container"><span class="select2-selection__placeholder">Destination city or airport</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                            </div>
                                                                                            <div class="thm-tk-input-15">
                                                                                                <label><asp:Label ID="Label13" Text="<%$Resources:Default.aspx,lblSDateFlight%>" meta:resourcekey="lblSDateFlight" runat="server" /></label>
                                                                                                <input type="text" name="outbounddate" class="thm-date-picker hasDatepicker" placeholder="Departure date" id="dp1530620289865">
                                                                                            </div>
                                                                                            <div class="thm-tk-input-15">
                                                                                                <label><asp:Label ID="Label12" Text="<%$Resources:Default.aspx,lblEDateFlight%>" meta:resourcekey="lblEDateFlight" runat="server" /></label>
                                                                                                <input type="text" name="inbounddate" class="thm-date-picker hasDatepicker" placeholder="Return date" id="dp1530620289866">
                                                                                            </div>
                                                                                            <div class="thm-tk-input-15">
                                                                                                <label><asp:Label ID="Label14" Text="<%$Resources:Default.aspx,lblClassFlight%>" meta:resourcekey="lblClassFlight" runat="server" /></label>
                                                                                                <select name="cabinclass" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                                                    <option value="Economy">Economy</option>
                                                                                                    <option value="PremiumEconomy">Premium Economy</option>
                                                                                                    <option value="Business">Business</option>
                                                                                                    <option value="First">First</option>
                                                                                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-cabinclass-38-container"><span class="select2-selection__rendered" id="select2-cabinclass-38-container" title="Economy">Economy</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                            </div>
                                                                                            <div class="thm-tk-input-15">
                                                                                                <div class="thm-tk-input-2-1">
                                                                                                    <label><asp:Label ID="lblAdlFlight1" Text="<%$Resources:Default.aspx,lblAdlFlight%>" meta:resourcekey="lblAdlFlight" runat="server" /></label>
                                                                                                    <select name="adults" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                                                        <option value="1">1</option>
                                                                                                        <option value="2">2</option>
                                                                                                        <option value="3">3</option>
                                                                                                        <option value="4">4</option>
                                                                                                        <option value="5">5</option>
                                                                                                        <option value="6">6</option>
                                                                                                        <option value="7">7</option>
                                                                                                        <option value="8">8</option>
                                                                                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-adults-fy-container"><span class="select2-selection__rendered" id="select2-adults-fy-container" title="1">1</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                                </div>
                                                                                                <div class="thm-tk-input-2-1">
                                                                                                    <label><asp:Label ID="lblKidFlight" Text="<%$Resources:Default.aspx,lblKidFlight%>" meta:resourcekey="lblKidFlight" runat="server" /></label>
                                                                                                    <select name="children" class="select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                                                                                        <option value="0">0</option>
                                                                                                        <option value="1">1</option>
                                                                                                        <option value="2">2</option>
                                                                                                        <option value="3">3</option>
                                                                                                        <option value="4">4</option>
                                                                                                        <option value="5">5</option>
                                                                                                        <option value="6">6</option>
                                                                                                        <option value="7">7</option>
                                                                                                        <option value="8">8</option>
                                                                                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: auto;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-children-xc-container"><span class="select2-selection__rendered" id="select2-children-xc-container" title="0">0</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <input type="hidden" name="country" value="US">
                                                                                            <input type="hidden" name="currency" value="USD">
                                                                                            <input type="hidden" name="locale" value="en-US">
                                                                                            <button class="btn btn-primary thm-tk-search-btn" id="flight-search" type="submit"><asp:Label ID="Label33" Text="<%$Resources:Default.aspx,lblSearch%>" meta:resourcekey="lblSearch" runat="server" /></button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="thm-tk-tab " id="vehicles">
                                                                                    <div class="thm-tk-tab-inner">
                                                                                        <form id="thm-tk-advancedsearch-form3" class="clearfix" action="http://almosafertrip.net/" method="GET">

                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label for="search-keyword"><asp:Label ID="lblFromCar" Text="<%$Resources:Default.aspx,lblFromCar%>" meta:resourcekey="lblFromCar" runat="server" /></label>
                                                                                                <input type="text" name="pickup" id="vehicles-pickup" class="thm-date-time-picker thm-tk-input-first hasDatepicker" placeholder="Pickup Date &amp; Time" value="" required="required">
                                                                                            </div>

                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label for="search-keyword"><asp:Label ID="Label19" Text="<%$Resources:Default.aspx,lblToCar%>" meta:resourcekey="lblToCar" runat="server" /></label>
                                                                                                <input type="text" name="pickup" id="vehicles-pickup" class="thm-date-time-picker hasDatepicker" placeholder="Pickup Date &amp; Time" value="" required="required">
                                                                                            </div>

                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label><asp:Label ID="Label21" Text="<%$Resources:Default.aspx,lblFromLocCar%>" meta:resourcekey="lblFromLocCar" runat="server" /></label>
                                                                                                <input type="text" name="pickup_location" placeholder="Pickup Location" value="" required="required">
                                                                                            </div>
                                                                                            <div class="thm-tk-input-4-1">
                                                                                                <label><asp:Label ID="Label30" Text="<%$Resources:Default.aspx,lblToLocCar%>" meta:resourcekey="lblToLocCar" runat="server" /></label>
                                                                                                <input type="text" name="drop_location" placeholder="Drop Location" value="" required="required">
                                                                                            </div>
                                                                                            <input type="hidden" name="post_type" value="vehicle">
                                                                                            <input type="hidden" name="s" value="">
                                                                                            <button class="btn btn-primary thm-tk-search-btn" type="submit"><asp:Label ID="Label31" Text="<%$Resources:Default.aspx,lblSearch%>" meta:resourcekey="lblSearch" runat="server" /></button>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>

                                                    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss = "";
                                                        if (htmlDiv) {
                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                        } else {
                                                            var htmlDiv = document.createElement("div");
                                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                        }
                                                    </script>
                                                    <script type="text/javascript">
                                                        setREVStartSize({ c: jQuery('#rev_slider_1_1'), responsiveLevels: [1240, 1024, 778, 480], gridwidth: [1140, 1024, 778, 480], gridheight: [1050, 768, 400, 400], sliderLayout: 'fullwidth' });

                                                        var revapi1,
                                                            tpj = jQuery;

                                                        tpj(document).ready(function () {
                                                            if (tpj("#rev_slider_1_1").revolution == undefined) {
                                                                revslider_showDoubleJqueryError("#rev_slider_1_1");
                                                            } else {
                                                                revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                                                    sliderType: "standard",
                                                                    jsFileLocation: "//almosafertrip.net/wp-content/plugins/revslider/public/assets/js/",
                                                                    sliderLayout: "fullwidth",
                                                                    dottedOverlay: "none",
                                                                    delay: 5500,
                                                                    navigation: {
                                                                        keyboardNavigation: "off",
                                                                        keyboard_direction: "horizontal",
                                                                        mouseScrollNavigation: "off",
                                                                        mouseScrollReverse: "default",
                                                                        onHoverStop: "off",
                                                                        arrows: {
                                                                            style: "zeus",
                                                                            enable: true,
                                                                            hide_onmobile: false,
                                                                            hide_onleave: false,
                                                                            tmp: '<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
                                                                            left: {
                                                                                h_align: "left",
                                                                                v_align: "center",
                                                                                h_offset: 30,
                                                                                v_offset: -80
                                                                            },
                                                                            right: {
                                                                                h_align: "right",
                                                                                v_align: "center",
                                                                                h_offset: 20,
                                                                                v_offset: -80
                                                                            }
                                                                        }
                                                                    },
                                                                    responsiveLevels: [1240, 1024, 778, 480],
                                                                    visibilityLevels: [1240, 1024, 778, 480],
                                                                    gridwidth: [1140, 1024, 778, 480],
                                                                    gridheight: [1050, 768, 400, 400],
                                                                    lazyType: "none",
                                                                    shadow: 0,
                                                                    spinner: "spinner0",
                                                                    stopLoop: "off",
                                                                    stopAfterLoops: -1,
                                                                    stopAtSlide: -1,
                                                                    shuffle: "off",
                                                                    autoHeight: "off",
                                                                    disableProgressBar: "on",
                                                                    hideThumbsOnMobile: "off",
                                                                    hideSliderAtLimit: 0,
                                                                    hideCaptionAtLimit: 0,
                                                                    hideAllCaptionAtLilmit: 0,
                                                                    debugMode: false,
                                                                    fallbacks: {
                                                                        simplifyAll: "off",
                                                                        nextSlideOnWindowFocus: "off",
                                                                        disableFocusListener: false,
                                                                    }
                                                                });
                                                            }

                                                        });	/*ready*/
                                                    </script>
                                                    <script>
                                                        var htmlDivCss = unescape("%23rev_slider_1_1%20.zeus.tparrows%20%7B%0A%20%20cursor%3Apointer%3B%0A%20%20min-width%3A70px%3B%0A%20%20min-height%3A70px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20display%3Ablock%3B%0A%20%20z-index%3A100%3B%0A%20%20border-radius%3A50%25%3B%20%20%20%0A%20%20overflow%3Ahidden%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.1%29%3B%0A%7D%0A%0A%23rev_slider_1_1%20.zeus.tparrows%3Abefore%20%7B%0A%20%20font-family%3A%20%22revicons%22%3B%0A%20%20font-size%3A27px%3B%0A%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20display%3Ablock%3B%0A%20%20line-height%3A%2070px%3B%0A%20%20text-align%3A%20center%3B%20%20%20%20%0A%20%20z-index%3A2%3B%0A%20%20position%3Arelative%3B%0A%7D%0A%23rev_slider_1_1%20.zeus.tparrows.tp-leftarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce824%22%3B%0A%7D%0A%23rev_slider_1_1%20.zeus.tparrows.tp-rightarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce825%22%3B%0A%7D%0A%0A%23rev_slider_1_1%20.zeus%20.tp-title-wrap%20%7B%0A%20%20background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20top%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20opacity%3A0%3B%0A%20%20transform%3Ascale%280%29%3B%0A%20%20-webkit-transform%3Ascale%280%29%3B%0A%20%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20-moz-transition%3Aall%200.3s%3B%0A%20%20%20border-radius%3A50%25%3B%0A%20%7D%0A%23rev_slider_1_1%20.zeus%20.tp-arr-imgholder%20%7B%0A%20%20width%3A100%25%3B%0A%20%20height%3A100%25%3B%0A%20%20position%3Aabsolute%3B%0A%20%20top%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20background-position%3Acenter%20center%3B%0A%20%20background-size%3Acover%3B%0A%20%20border-radius%3A50%25%3B%0A%20%20transform%3Atranslatex%28-100%25%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-100%25%29%3B%0A%20%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20-moz-transition%3Aall%200.3s%3B%0A%0A%20%7D%0A%23rev_slider_1_1%20.zeus.tp-rightarrow%20.tp-arr-imgholder%20%7B%0A%20%20%20%20transform%3Atranslatex%28100%25%29%3B%0A%20%20-webkit-transform%3Atranslatex%28100%25%29%3B%0A%20%20%20%20%20%20%7D%0A%23rev_slider_1_1%20.zeus.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20transform%3Atranslatex%280%29%3B%0A%20%20-webkit-transform%3Atranslatex%280%29%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%20%20%20%20%20%20%0A%23rev_slider_1_1%20.zeus.tparrows%3Ahover%20.tp-title-wrap%20%7B%0A%20%20transform%3Ascale%281%29%3B%0A%20%20-webkit-transform%3Ascale%281%29%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%20%0A");
                                                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                                        if (htmlDiv) {
                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                        }
                                                        else {
                                                            var htmlDiv = document.createElement('div');
                                                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                                        }
                                                    </script>
                                                </div>
                                                <!-- END REVOLUTION SLIDER -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid show-overflow package-search-wrapper vc_custom_1505974672499 vc_row-has-fill">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">

                                        <div class="thm-tk-search">
                                            <div class="thm-tk-search-nav">
                                                <ul>
                                                    <li><a href="#holidays" class="active"><i class="icon-location"></i><asp:Label ID="Label1" runat="server" Text="<%$Resources:Default.aspx,lblHoliday%>" meta:resourcekey="lblHoliday">Holidays</asp:Label></a></li>
                                                    <li><a href="#hotel" class=""><i class="icon-hotel"></i><asp:Label ID="lblHotel" runat="server" Text="<%$Resources:Default.aspx,lblHotel%>" meta:resourcekey="lblHotel">Hotels &amp; Resorts</asp:Label></a></li>
                                                    <li><a href="#flights"><i class="icon-flight"></i><asp:Label ID="lblFlight" runat="server" Text="<%$Resources:Default.aspx,lblFlight%>" meta:resourcekey="lblFlight">Flights</asp:Label></a></li>
                                                    <li><a href="#vehicles" class=""><i class="icon-vehicles"></i><asp:Label ID="lblVehicles" runat="server" Text="<%$Resources:Default.aspx,lblVehicles%>" meta:resourcekey="lblVehicles">Vehicles</asp:Label></a></li>
                                                </ul>
                                            </div>
                                            <div class="thm-tk-search-form">
                                                <div class="thm-tk-tab active" id="holidays">
                                                    <div class="thm-tk-tab-inner clearfix">
                                                        <form id="thm-tk-advancedsearch-form" class="clearfix" action="http://almosafertrip.net/" method="GET">
                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="Label2"  Text="<%$Resources:Default.aspx,lblSearchHol%>" meta:resourcekey="lblSearchHol" runat="server" /></label>
                                                                <input type="text" name="s" class="thm-tk-input-first" placeholder="Type keyword" value="">
                                                            </div>

                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="lblLocHol"  Text="<%$Resources:Default.aspx,lblLocHol%>" meta:resourcekey="lblLocHol" runat="server" /></label>
                                                                <select name="location" class="select2">
                                                                    <option value="">Select a Location</option>
                                                                    <option value="america">America</option>
                                                                    <option value="australia">Australia</option>
                                                                    <option value="bangladesh">Bangladesh</option>
                                                                    <option value="dhaka">Dhaka</option>
                                                                    <option value="france">France</option>
                                                                    <option value="india">India</option>
                                                                    <option value="london">London</option>
                                                                    <option value="russia">Russia</option>
                                                                </select>
                                                            </div>
                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="Label3"  Text="<%$Resources:Default.aspx,lblChkInHol%>" meta:resourcekey="lblChkInHol" runat="server" /></label>
                                                                <input type="text" name="checkin" class="thm-date-picker" value="" placeholder="Check-in date">
                                                            </div>
                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="Label4" Text="<%$Resources:Default.aspx,lblGuestHol%>" meta:resourcekey="lblGuestHol" runat="server" /></label>
                                                                <input type="number" name="guest" class="" value="" placeholder="Number of guests">
                                                            </div>
                                                            <input type="hidden" name="post_type" value="package">
                                                            <button class="btn btn-primary thm-tk-search-btn" type="submit"><asp:Label ID="Label37" Text="<%$Resources:Default.aspx,lblSearch%>" meta:resourcekey="lblSearch" runat="server" /></button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="thm-tk-tab " id="hotel">
                                                    <div class="thm-tk-tab-inner">
                                                        <form id="thm-tk-advancedsearch-form2" class="clearfix" action="http://almosafertrip.net/" method="GET">
                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="Label5" Text="<%$Resources:Default.aspx,lblSearchHot%>" meta:resourcekey="lblSearchHot" runat="server" /></label>
                                                                <input type="text" name="s" id="search-keyword" class="thm-tk-input-first" placeholder="Type keyword" value="">
                                                            </div>

                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="lblLocationHot" Text="<%$Resources:Default.aspx,lblLocationHot%>" meta:resourcekey="lblLocationHot" runat="server" /></label>
                                                                <select name="hotel_location" class="select2">
                                                                    <option value="">Select a location</option>
                                                                    <option value="abu-dhabi">Abu Dhabi</option>
                                                                    <option value="anguilla">Anguilla</option>
                                                                    <option value="bahamas">Bahamas</option>
                                                                    <option value="dhaka">dhaka</option>
                                                                    <option value="dubai">Dubai</option>
                                                                    <option value="german">German</option>
                                                                    <option value="las-vegas">Las Vegas</option>
                                                                    <option value="london">London</option>
                                                                    <option value="los-cabos">Los Cabos</option>
                                                                    <option value="new-york-city">New York City</option>
                                                                    <option value="paris">Paris</option>
                                                                    <option value="rome">Rome</option>
                                                                    <option value="trivago">Trivago</option>
                                                                    <option value="turkey">Turkey</option>
                                                                </select>
                                                            </div>
                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="Label7" Text="<%$Resources:Default.aspx,lblTypeHot%>" meta:resourcekey="lblTypeHot" runat="server" /></label>
                                                                <select name="hotel_category" class="select2">
                                                                    <option value="">Hotel Type</option>
                                                                    <option value="3-star">3 Star</option>
                                                                    <option value="5-star">5 Star</option>
                                                                    <option value="hotel">Hotel</option>
                                                                </select>
                                                            </div>
                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="lblRoomCatHot" Text="<%$Resources:Default.aspx,lblRoomCatHot%>" meta:resourcekey="lblRoomCatHot" runat="server" /></label>
                                                                <select name="room_type" class="select2">
                                                                    <option value="">Room Type</option>
                                                                    <option value="doubleroom">Double Room</option>
                                                                    <option value="singleroom">Single Room</option>
                                                                    <option value="luxuryroom">Luxury Room</option>
                                                                    <option value="generalroom">General Room</option>
                                                                    <option value="familyroom">Family Room</option>
                                                                    <option value="deluxeroom">Deluxe Room</option>
                                                                </select>
                                                            </div>
                                                            <input type="hidden" name="post_type" value="hotel">
                                                            <button class="btn btn-primary thm-tk-search-btn" type="submit"><asp:Label ID="Label36" Text="<%$Resources:Default.aspx,lblSearch%>" meta:resourcekey="lblSearch" runat="server" /></button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="thm-tk-tab" id="flights">
                                                    <div class="thm-tk-tab-inner">
                                                        <form id="thm-tk-flights-search-form" class="clearfix" data-tp="0">
                                                            <div class="thm-tk-input-5-1 thm-tk-first-select">
                                                                <label><asp:Label ID="lblFromFlight" Text="<%$Resources:Default.aspx,lblFromFlight%>" meta:resourcekey="lblFromFlight" runat="server" /></label>
                                                                <select name="originplace" class="thm-fs-place" data-placeholder="Origin city or airport"></select>
                                                            </div>
                                                            <div class="thm-tk-input-5-1">
                                                                <label><asp:Label ID="lblToFlight" Text="<%$Resources:Default.aspx,lblToFlight%>" meta:resourcekey="lblToFlight" runat="server" /></label>
                                                                <select name="destinationplace" class="thm-fs-place" data-placeholder="Destination city or airport"></select>
                                                            </div>
                                                            <div class="thm-tk-input-15">
                                                                <label><asp:Label ID="Label10" Text="<%$Resources:Default.aspx,lblSDateFlight%>" meta:resourcekey="lblSDateFlight" runat="server" /></label>
                                                                <input type="text" name="outbounddate" class="thm-date-picker" placeholder="Departure date">
                                                            </div>
                                                            <div class="thm-tk-input-15">
                                                                <label><asp:Label ID="Label11" Text="<%$Resources:Default.aspx,lblEDateFlight%>" meta:resourcekey="lblEDateFlight" runat="server" /></label>
                                                                <input type="text" name="inbounddate" class="thm-date-picker" placeholder="Return date">
                                                            </div>
                                                            <div class="thm-tk-input-15">
                                                                <label><asp:Label ID="Label16" Text="<%$Resources:Default.aspx,lblClassFlight%>" meta:resourcekey="lblClassFlight" runat="server" /></label>
                                                                <select name="cabinclass" class="select2">
                                                                    <option value="Economy">Economy</option>
                                                                    <option value="PremiumEconomy">Premium Economy</option>
                                                                    <option value="Business">Business</option>
                                                                    <option value="First">First</option>
                                                                </select>
                                                            </div>
                                                            <div class="thm-tk-input-15">
                                                                <div class="thm-tk-input-2-1">
                                                                    <label><asp:Label ID="lblAdlFlight" Text="<%$Resources:Default.aspx,lblAdlFlight%>" meta:resourcekey="lblAdlFlight" runat="server" /></label>
                                                                    <select name="adults" class="select2">
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                    </select>
                                                                </div>
                                                                <div class="thm-tk-input-2-1">
                                                                    <label><asp:Label ID="Label17" Text="<%$Resources:Default.aspx,lblKidFlight%>" meta:resourcekey="lblKidFlight" runat="server" /></label>
                                                                    <select name="children" class="select2">
                                                                        <option value="0">0</option>
                                                                        <option value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="country" value="US">
                                                            <input type="hidden" name="currency" value="USD">
                                                            <input type="hidden" name="locale" value="en-US">
                                                            <button class="btn btn-primary thm-tk-search-btn" id="flight-search" type="submit"><asp:Label ID="Label35" Text="<%$Resources:Default.aspx,lblSearch%>" meta:resourcekey="lblSearch" runat="server" /></button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="thm-tk-tab " id="vehicles">
                                                    <div class="thm-tk-tab-inner">
                                                        <form id="thm-tk-advancedsearch-form3" class="clearfix" action="http://almosafertrip.net/" method="GET">

                                                            <div class="thm-tk-input-4-1">
                                                                <label for="search-keyword"><asp:Label ID="Label15" Text="<%$Resources:Default.aspx,lblFromCar%>" meta:resourcekey="lblFromCar" runat="server" /></label>
                                                                <input type="text" name="pickup" id="vehicles-pickup" class="thm-date-time-picker thm-tk-input-first" placeholder="Pickup Date &amp; Time" value="" required="required">
                                                            </div>

                                                            <div class="thm-tk-input-4-1">
                                                                <label for="search-keyword"><asp:Label ID="Label18" Text="<%$Resources:Default.aspx,lblToCar%>" meta:resourcekey="lblToCar" runat="server" /></label>
                                                                <input type="text" name="droptime" id="vehicles-dropoff" class="thm-date-time-picker" placeholder="drop off Date &amp; Time" value="" required="required">
                                                            </div>

                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="Label20" Text="<%$Resources:Default.aspx,lblFromLocCar%>" meta:resourcekey="lblFromLocCar" runat="server" /></label>
                                                                <input type="text" name="pickup_location" placeholder="Pickup Location" value="" required="required">
                                                            </div>
                                                            <div class="thm-tk-input-4-1">
                                                                <label><asp:Label ID="Label23" Text="<%$Resources:Default.aspx,lblToLocCar%>" meta:resourcekey="lblToLocCar" runat="server" /></label>
                                                                <input type="text" name="drop_location" placeholder="Drop Location" value="" required="required">
                                                            </div>
                                                            <input type="hidden" name="post_type" value="vehicle">
                                                            <input type="hidden" name="s" value="">
                                                            <button class="btn btn-primary thm-tk-search-btn" type="submit"><asp:Label ID="Label34" Text="<%$Resources:Default.aspx,lblSearch%>" meta:resourcekey="lblSearch" runat="server" /></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1475230988197 vc_row-has-fill">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="thm-flight-results">
                                            <div class="alert thm-flight-alert alert-danger" role="alert">There is some error to get flight data. Please try again.</div>
                                            <div class="alert thm-flight-alert-no-result alert-danger" role="alert">Sorry, no result found.</div>
                                            <div class="flight-loader">
                                                <div class="loader">Loading...</div>
                                                Please wait. It may take up to 2 mins.
                                            </div>
                                            <h2 class="thm-titlestandardstyle"><span class="thm-flight-count"></span>Flights Found</h2>
                                            <ul id="all-flights">
                                                <li data-pos="0"></li>
                                                <li data-pos="1"></li>
                                                <li data-pos="2"></li>
                                                <li data-pos="3"></li>
                                                <li data-pos="4"></li>
                                                <li data-pos="5"></li>
                                                <li data-pos="6"></li>
                                                <li data-pos="7"></li>
                                                <li data-pos="8"></li>
                                                <li data-pos="9"></li>
                                            </ul>
                                            <ul id="flight-pagination"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid thm-res-padding vc_custom_1477474043462 vc_row-has-fill">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_center">

                                                            <figure class="wpb_wrapper vc_figure">
                                                                <a href="http://almosafertrip.net/e-member/Login.aspx" target="_blank" class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    <img width="300" height="50" src="wp-content/uploads/2016/07/11.png" class="vc_single_image-img attachment-full" alt="" /></a>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_single_image wpb_content_element vc_align_center">

                                                            <figure class="wpb_wrapper vc_figure">
                                                                <a href="http://almosafertrip.net/payment/" target="_blank" class="vc_single_image-wrapper   vc_box_border_grey">
                                                                    <img width="300" height="50" src="wp-content/uploads/2016/07/payment.png" class="vc_single_image-img attachment-full" alt="" /></a>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                        <div class="addon-themeum-title " style="text-align: center;">
                                            <div class="themeum-titlelayout1">
                                                <h2 class="thm-titlestandardstyle">
                                                    <asp:Label runat="server" ID="lblTrialResource1" Text="<%$Resources:Default.aspx,lblTrialResource1%>" meta:resourcekey="lblTrialResource1"></asp:Label>
                                                </h2>
                                                <p class="thm-sub-titlestandardstylesub"><asp:Literal runat="server" ID="Label26" Text="<%$Resources:Default.aspx,lblChoosenPack%>" meta:resourcekey="lblChoosenPack" /></p>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                        <div class="add-popular-tour-package">
                                            <div class="row">
                                                <asp:ListView ID="lvPackages" ClientIDMode="Static" runat="server">
                                                    <ItemTemplate>
                                                        <div class="col-sm-6 col-md-4">
                                                            <div class="package-list-wrap ">
                                                                <img width="570" height="400" src="<%# Eval("url") %>" class="img-responsive wp-post-image" alt="" /><div class="package-list-content">
                                                                    <p class="package-list-duration"><%# Eval("textDesc") %></p>
                                                                    <h3 class="package-list-title"><a href="tourPackage.aspx?ID=<%# Eval("tourID") %>"><%# Eval("name") %></a></h3>
                                                                    <a class="package-list-button" href="tourPackage.aspx?ID=<%# Eval("tourID") %>">
                                                                        <asp:Label runat="server" ID="lblDetalis" Text="<%$Resources:Default.aspx,lblDetalis%>" meta:resourcekey="lblDetalis"></asp:Label>
                                                                        </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" data-vc-parallax="1.5" data-vc-parallax-o-fade="on" data-vc-parallax-image="https://www.youtube.com/watch?v=lMJXxhRFO1k" class="vc_row wpb_row vc_row-fluid thm-res-padding vc_custom_1476436016777 vc_row-has-fill vc_row-no-padding vc_video-bg-container vc_general vc_parallax vc_parallax-content-moving-fade js-vc_parallax-o-fade">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="addon-themeum-action whitecolor" style="text-align: center;">
                                            <div class="themeum-action">
                                                <h2 class="action-titlestandardstyle">
                                                    <asp:Label ID="lblTitleOff" Text="<%$Resources:Default.aspx,lblTitleOff%>" meta:resourcekey="lblTitleOff" runat="server" /></h2>
                                                <p class="action-sub-titlestandardstylesub">
                                                    <asp:Label ID="Label24" Text="<%$Resources:Default.aspx,lblSubTitleOff%>" meta:resourcekey="lblSubTitleOff" runat="server" />
                                                    
                                                </p>
                                                <p><a class="action-btn btn btn-success btn-lg" href="#" target="_blank"><asp:Label ID="Label25" Text="<%$Resources:Default.aspx,lblBtnOff%>" meta:resourcekey="lblBtnOff" runat="server" /></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="vc_row wpb_row vc_row-fluid thm-res-margin vc_custom_1476436987912">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                        <div class="addon-themeum-title " style="text-align: center;">
                                            <div class="themeum-titlelayout1">
                                                <h2 class="thm-titlestandardstyle">
                                                      <asp:Label runat="server" ID="lbldestin" Text="<%$Resources:Default.aspx,lbldestin%>" meta:resourcekey="lbldestin"></asp:Label>
                                                    </h2>
                                            </div>
                                        </div>
                                        <div class="package-location-shortcode "></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid thm-res-padding vc_custom_1476437088236 vc_row-has-fill">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="addon-themeum-title whitecolor" style="text-align: center;">
                                            <div class="themeum-titlelayout1">
                                                <h2 class="thm-titlestandardstyle">
                                                    <asp:Label runat="server" ID="lblmemb" Text="<%$Resources:Default.aspx,lblmemb%>" meta:resourcekey="lblmemb"></asp:Label>
                                                </h2>
                                                <p class="thm-sub-titlestandardstylesub">
                                                    <asp:Label runat="server" ID="lblship" Text="<%$Resources:Default.aspx,lblship%>" meta:resourcekey="lblship"></asp:Label>
                                                    </p>
                                            </div>
                                        </div>
                                        <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid thm-res-padding vc_custom_1476437176473 vc_row-has-fill">
                            <div class="wpb_column vc_column_container vc_col-sm-2">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-8">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="addon-themeum-title whitecolor themeum-titlelayout-custom" style="text-align: center;">
                                            <div class="themeum-titlelayout1">
                                                <h2 class="thm-titlestandardstyle">
                                                    <asp:Label runat="server" ID="lblsub" Text="<%$Resources:Default.aspx,lblsub%>" meta:resourcekey="lblsub"></asp:Label>
                                                   </h2>
                                            </div>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element  themeum-newsletter">
                                            <div class="wpb_wrapper">
                                                <script type="text/javascript">(function () {
    if (!window.mc4wp) {
        window.mc4wp = {
            listeners: [],
            forms: {
                on: function (event, callback) {
                    window.mc4wp.listeners.push({
                        event: event,
                        callback: callback
                    });
                }
            }
        }
    }
})();
                                                </script>
                                                <!-- MailChimp for WordPress v4.1.14 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
                                                <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-139" method="post" data-id="139" data-name="">
                                                    <div class="mc4wp-form-fields">
                                                        <p>
                                                            <input type="email" name="EMAIL" placeholder="<asp:Literal runat='server' Text='<%$ Resources:Default.aspx,lblmailPH%>' meta:resourcekey='lblmailPH' />"  />
<%--                                                            <input type="email" name="EMAIL" placeholder="Your Email Address" required />--%>
                                                            <button class="btn btn-primary thm-tk-search-btn" type="submit"><asp:Label ID="Label41" Text="<%$Resources:Default.aspx,lblSubmit%>" meta:resourcekey="lblSubmit" runat="server" /></button>
                                                        </p>
                                                        <label style="display: none !important;">
                                                            Leave this field empty if you're human:
                                                            <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" /></label><input type="hidden" name="_mc4wp_timestamp" value="1530544885" /><input type="hidden" name="_mc4wp_form_id" value="139" /><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1" />
                                                    </div>
                                                    <div class="mc4wp-response"></div>
                                                </form>
                                                <!-- / MailChimp for WordPress Plugin -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-2">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                    </div>

                </div>


            </div>
            <!--/#content-->
        </div>
        <!--/container-->
    </section>

    <%--Start Alert--%>
    <asp:Panel ID="myAlert" runat="server">
        <h2 class="text-center"><a href="default.aspx"><asp:Literal ID="Literal3" Text="<%$Resources:General.aspx,lblHomePg%>" meta:resourcekey="lblHomePg" runat="server" /></a></h2>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> <asp:Label ID="lblAlert"  runat="server" Text="<%$Resources:Default.aspx,lblAlert%>" meta:resourcekey="lblAlert" />!</strong>
        </div>
        <div class="modalBG alert-dismissible" style="
            min-height: 1602px;
            position: fixed;
            width: 100%;
            top: 0;
            left: 0;
            background: rgba(0,0,0,0.8);
            z-index: -1;
        "></div>
    </asp:Panel>
    <%--End Alert--%>

    <!--/#main-->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
    <script>
        $(".thm-tk-search-nav ul li a i").click(function () {

            var tab = $(this);
            var anchor = $(tab).closest("a").attr('href');

            if ($(".thm-tk-tab").hasClass('active')) {
                $(".thm-tk-tab").removeClass('active');
            }
            var targetStr = ".thm-tk-tab" + anchor;
            //alert(targetStr);
            var target = $(targetStr);
            target.addClass('active');
        });
    </script>
    <script>
        $(".thm-tk-search-nav ul li a span").click(function () {

            var tab = $(this);
            var anchor = $(tab).closest("a").attr('href');

            if ($(".thm-tk-tab").hasClass('active')) {
                $(".thm-tk-tab").removeClass('active');
            }
            var targetStr = ".thm-tk-tab" + anchor;
            //alert(targetStr);
            var target = $(targetStr);
            target.addClass('active');
        });
    </script>
    <script>
        $(document).ready(function () {

            
            $(".carousel-item.slider-fullscreen-image:nth-child(1)").addClass('active');
        });
    </script>
</asp:Content>
