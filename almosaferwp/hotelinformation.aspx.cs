﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace almosaferwp
{
    public partial class hotelinformation : basePage
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    loadHotelInfo();
                    myAlert.Visible = false;
                }
            }
            catch 
            {

                myAlert.Visible = true;
            }
            
        }
        protected void loadHotelInfo()
        {
            string tbltourHotel = "tourHotel";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tbltourHotel = "tourHotelAr";

            }
            DataSet dsHtl = codeClass.Sqlread("select * from " + tbltourHotel + " where ID = " + Request.QueryString["ID"].ToString());
            lblHotelName.Text = dsHtl.Tables[0].Rows[0]["name"].ToString();
            lvHotelInfo.DataSource = dsHtl;
            lvHotelInfo.DataBind();
        }
        protected void loadHotelRelated()
        {
            string tbltourHotel = "tourHotel";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tbltourHotel = "tourHotelAr";
            }
            ListView lvCont = (ListView)lvHotelInfo;
            ListViewDataItem lvContItem = (ListViewDataItem)lvCont.FindControl("ctrl0");
            ListView lvRelatedHotels = (ListView)lvContItem.FindControl("lvRelatedHotels");
            DataSet dsHtl = codeClass.Sqlread("select top (6) * from " + tbltourHotel + " where ID <> " + Request.QueryString["ID"].ToString());
            lvRelatedHotels.DataSource = dsHtl;
            lvRelatedHotels.DataBind();
        }
        protected void loadHotelRoom()
        {
            string tblhotelRoom = "hotelRoom";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblhotelRoom = "hotelRoomAr";

            }
            ListView lvCont = (ListView)lvHotelInfo;
            ListViewDataItem lvContItem = (ListViewDataItem)lvCont.FindControl("ctrl0");
            ListView lvRoom = (ListView)lvContItem.FindControl("lvRoom");
            DataSet dsRoom = codeClass.Sqlread("select * from " + tblhotelRoom + " where hotelID = " + Request.QueryString["ID"].ToString());
            lvRoom.DataSource = dsRoom;
            lvRoom.DataBind();
        }
        protected void loadHotelFacilities()
        {
            string tblhotelFacility = "hotelFacility";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblhotelFacility = "hotelFacilityAr";

            }
            ListView lvCont = (ListView)lvHotelInfo;
            ListViewDataItem lvContItem = (ListViewDataItem)lvCont.FindControl("ctrl0");
            ListView lvHtlFacilities = (ListView)lvContItem.FindControl("lvHtlFacilities");
            DataSet dsFacil = codeClass.Sqlread("select * from " + tblhotelFacility + " where hotelID = " + Request.QueryString["ID"].ToString());
            lvHtlFacilities.DataSource = dsFacil;
            lvHtlFacilities.DataBind();
        }

        protected void lvHotelInfo_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            
            loadHotelRoom();
            loadHotelRelated();
            loadHotelFacilities();              
           
        }
    }
}