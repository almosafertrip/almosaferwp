﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="package_list.aspx.cs" Inherits="AlmosaferWP.package_list" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal ID="lblTitlePack" Text="<%$Resources:General.aspx,lblTitlePack%>" meta:resourcekey="lblTitlePack" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        #main {
            background-color: white !important;
        }

        /* alert style */
        #CPContent_myAlert {
        
            width: 80%;
            margin: auto;
            position: fixed;
            left: 10.5%;
            top: 20%;
            height: auto;
            word-break: break-word;
            z-index: 4444;

        }
        #CPContent_myAlert div {
            
            height: inherit;
            font-size: large;
            text-align: center;
            border: black 1px solid;

        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">


    <div class="subtitle-cover sub-title " style="background-image: url(covers/81797.jpg); background-size: cover; background-position: 50% 50%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <h2 class="page-leading"><asp:Literal ID="Literal1" Text="<%$Resources:General.aspx,lblTitlePack%>" meta:resourcekey="lblTitlePack" runat="server" /></h2>
                    <ol class="breadcrumb">
                        <li><a href="Default.aspx" class="breadcrumb_home"><asp:Literal ID="Literal3" Text="<%$Resources:General.aspx,lblHome%>" meta:resourcekey="lblHome" runat="server" /></a></li>
                        <li class="active"><asp:Literal ID="Literal2" Text="<%$Resources:General.aspx,lblTitlePack%>" meta:resourcekey="lblTitlePack" runat="server" />            </li>
                    </ol>

                </div>
            </div>
        </div>
    </div>
    <!--/.sub-title-->
    <section id="main" class="clearfix">
        <div class="container">
            <div class="row">
                <div id="content" class="courses col-md-12" role="main">
                    <div class="row">
                        <asp:ListView ID="lvPackages" ClientIDMode="Static" runat="server">
                            <ItemTemplate>
                                <div class="col-sm-6 col-md-4">
                                    <div class="package-list-wrap ">
                                        <img width="570" height="400" src="<%# Eval("url") %>" class="img-responsive wp-post-image" alt="" /><div class="package-list-content">
                                            <p class="package-list-duration"><%# Eval("textDesc") %></p>
                                            <h3 class="package-list-title"><a href="tourPackage.aspx?ID=<%# Eval("tourID") %>"><%# Eval("name") %></a></h3>
                                            <a class="package-list-button" href="tourPackage.aspx?ID=<%# Eval("tourID") %>">
                                                <asp:Label runat="server" ID="lblDetalis1" Text="<%$Resources:Default.aspx,lblDetalis1%>" meta:resourcekey="lblDetalis1"></asp:Label>
                                                </a>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <%--<div class="themeum-pagination">
                        <ul class='page-numbers'>
                            <li><span class='page-numbers current'>1</span></li>
                            <li><a class='page-numbers' href='page/2/index.html'>2</a></li>
                            <li><a class="next page-numbers" href="page/2/index.html">Next &raquo;</a></li>
                        </ul>
                    </div>--%>
                </div>
            </div>
        </div>
    </section>
    <!-- start footer -->


    <%--Start Alert--%>
    <asp:Panel ID="myAlert" runat="server">
        <h2 class="text-center"><a href="default.aspx"><asp:Literal ID="Literal4" Text="<%$Resources:General.aspx,lblHomePg%>" meta:resourcekey="lblHomePg" runat="server" /></a></h2>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> <asp:Label ID="lblAlert"  runat="server" Text="<%$Resources:Default.aspx,lblAlert%>" meta:resourcekey="lblAlert" />!</strong>
        </div>
        <div class="modalBG alert-dismissible" style="
            min-height: 1602px;
            position: fixed;
            width: 100%;
            top: 0;
            left: 0;
            background: rgba(0,0,0,0.8);
            z-index: -1;
        "></div>
    </asp:Panel>
    <%--End Alert--%>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
