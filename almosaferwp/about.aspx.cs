﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using almosaferwp;
using System.Data;
using System.Web.UI.HtmlControls;


namespace AlmosaferWP
{
    public partial class about : basePage
    {
        CodeClass codeClass = new CodeClass();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                InitializeCulture();

                if (!IsPostBack)
                {
                    loadwork();
                    loadHWDI();
                  
                }
            }
            catch
            {
                
            }

        }

    
    protected void loadwork()
        {
            string tblName = "about";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "aboutAr";
            }
            DataSet dswork = codeClass.Sqlread("select * from " + tblName);
            litrabout.Text = dswork.Tables[0].Rows[0]["about"].ToString();
            litrabout1.Text = dswork.Tables[0].Rows[0]["about"].ToString();
            lblwho.Text = dswork.Tables[0].Rows[0]["whoWe"].ToString();
            lblour.Text = dswork.Tables[0].Rows[0]["ourMission"].ToString();
            lblparg1.Text = dswork.Tables[0].Rows[0]["pargraph1"].ToString();
            lblparg2.Text = dswork.Tables[0].Rows[0]["pargraph2"].ToString();
            lblweDoit2.Text = dswork.Tables[0].Rows[0]["weDoit2"].ToString();
            //lblpargraph5.Text = dswork.Tables[0].Rows[0]["pargraph5"].ToString();
            lblweDoit.Text = dswork.Tables[0].Rows[0]["weDoit"].ToString();
            lblpargraph4.Text = dswork.Tables[0].Rows[0]["pargraph4"].ToString();
            lblimportant.Text = dswork.Tables[0].Rows[0]["important"].ToString();
            lblpargraph3.Text = dswork.Tables[0].Rows[0]["pargraph3"].ToString();
            ContentPlaceHolder myCP = (ContentPlaceHolder)Page.Master.FindControl("CPContent");
            HtmlImage img = (HtmlImage)myCP.FindControl("lblurlimg2");
            img.Attributes["src"] = ResolveUrl(dswork.Tables[0].Rows[0]["urlimg2"].ToString());

            lvwork.DataSource = dswork;
            lvwork.DataBind();
        }
        protected void loadHWDI()
        {
            string tblName = "HWDI";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "HWDIAr";
            }
            DataSet ds = codeClass.Sqlread("select * from  " + tblName);
            lvHWDI.DataSource = ds;
            lvHWDI.DataBind();
        }
    }
}