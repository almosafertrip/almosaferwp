﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="tourPackage.aspx.cs" Inherits="almosaferwp.tourPackage" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal Text="Tour Package" ID="lblTitle" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        body #buynowbtn {
            display: none !important;
        }
        /*.packagedetailin, .recTours {
            margin : 20px auto !important; 
        }*/
        #main {
            background-color : white !important; 
        }
        /* modal styles*/
         .modalBackground
         {
             background-color: Black;
             filter: alpha(opacity=90);
             opacity: 0.8;
         }
         #CPContent_Panel1 input[type=text] {
             color:black !important;
         }
         .modalPopup
         {
             background-color: #FFFFFF;
             border-width: 3px;
             border-style:  dotted;
             border-color: black;
             padding: 5%;
             width: 100%;
             height: 80%;
             position:fixed;
             margin:5% 8%;
         }
         /* modal styles*/

         /* alert style */
        #CPContent_myAlert {
        
            width: 80%;
            margin: auto;
            position: fixed;
            left: 10.5%;
            top: 20%;
            height: auto;
            word-break: break-word;
            z-index: 4444;

        }
        #CPContent_myAlert div {
            
            height: inherit;
            font-size: large;
            text-align: center;
            border: black 1px solid;

        }
    </style>
    <script src="assets/web/assets/jquery/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <cc1:ModalPopupExtender ID="mp1" runat="server" PopupControlID="Panel1" TargetControlID="fakeBtn" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:Button ID="fakeBtn" runat="server" Visible="false" />
    <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup col-sm-10" align="center" style="display:none;" >
        <div class="col-xs-12 marginTop">
            <div class="col-md-8 col-md-offset-2 to-animate-2">
				<h3>
                    <asp:Label ID="lblkindly" runat="server" Text="<%$Resources:tourPackage.aspx,lblkindly%>" meta:resourcekey="lblkindly"></asp:Label> 
				</h3>
                
				<div class="form-group ">
					<label for="name" class="sr-only">
                       
                        </label>
					<asp:TextBox runat="server" ID="txtName" class="form-control" placeholder="<%$Resources:tourPackage.aspx,lblname%>" meta:resourcekey="lblname" >

					</asp:TextBox>
				</div>
				<div class="form-group ">
					<label for="txtEmail" class="sr-only">Email</label>
					<asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" placeholder="<%$Resources:tourPackage.aspx,lblEmail%>" meta:resourcekey="lblEmail" ></asp:TextBox>
				</div>
                <%--<div class="form-group ">
					<label for="txtEmail" class="sr-only">Email</label>
					<asp:TextBox runat="server" ID="txtToEmail" CssClass="form-control" placeholder="Emailed To" ></asp:TextBox>
				</div>--%>
						
				<div class="form-group ">
					<label for="message" class="sr-only">Message</label>
					<asp:TextBox runat="server" TextMode="MultiLine" ID="txtDescription" cols="30" rows="5" CssClass="form-control textBox" placeholder="<%$Resources:tourPackage.aspx,lblQuick%>" meta:resourcekey="lblQuick" ></asp:TextBox>
				</div>
					
            </div>
<%--     <asp:Label ID="Label1" runat="server" Text="<%$Resources:tourPackage.aspx,lblkindly%>" meta:resourcekey="lblkindly"></asp:Label> --%>
            <br />
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary marginTopSm" OnClick="btnSubmit_Click" Text="<%$Resources:tourPackage.aspx,lblbtnsub%>" meta:resourcekey="lblbtnsub" />
            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-secondary marginTopSm" Text="<%$Resources:tourPackage.aspx,lblclose%>" meta:resourcekey="lblclose" />
        </div>
    </asp:Panel>
    <section id="main" class="package-single clearfix">
        <!-- buy now button popup -->
        <asp:ListView ID="lvTourPack" ClientIDMode="Static" OnItemDataBound="lvTourPack_ItemDataBound" runat="server">
            <ItemTemplate>
                <div id="buynowbtn" class="white-popup mfp-with-anim mfp-hide">
                    <h3 class="title">Booking for : <span>Paris city Tour</span></h3>
                    <div role="form" class="wpcf7" id="wpcf7-f416-p323-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <asp:Panel CssClass="wpcf7-form" runat="server">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="416">
                                <input type="hidden" name="_wpcf7_version" value="4.9.2">
                                <input type="hidden" name="_wpcf7_locale" value="en_US">
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f416-p323-o1">
                                <input type="hidden" name="_wpcf7_container_post" value="323">
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="btnfield_name">Name: <span class="required">*</span></label><br>
                                    <span class="wpcf7-form-control-wrap name">
                                        <input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="btnfield_name" aria-required="true" aria-invalid="false"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="btnfield_email">Email: <span class="required">*</span></label><br>
                                    <span class="wpcf7-form-control-wrap email">
                                        <input type="text" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="btnfield_email" aria-required="true" aria-invalid="false"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="btnfield_phone">Phone: <span class="required">*</span></label><br>
                                    <span class="wpcf7-form-control-wrap phone">
                                        <input type="text" name="phone" value="" size="40" class="wpcf7-form-control wpcf7-text form-control" id="btnfield_phone" aria-invalid="false"></span>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="btnfield_price">Price:</label><br>
                                    <span class="wpcf7-form-control-wrap price">
                                        <input type="text" name="price" value="" size="40" class="wpcf7-form-control wpcf7-text form-control" id="btnfield_price" aria-invalid="false"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="btnfield_ext_info">Extra Information:</label><br>
                                    <span class="wpcf7-form-control-wrap ext_info">
                                        <textarea name="ext_info" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" id="btnfield_ext_info" aria-invalid="false"></textarea></span>
                                </div>
                            </div>
                            <p>
                                <input type="submit" value="Book By E-Mail" class="wpcf7-form-control wpcf7-submit buybtnemail btn btn-success btn-lg btn-block"><span class="ajax-loader"></span></p>
                            <div class="text-center">or</div>
                            <p>
                                <input type="button" value="Check Out Via PayPal" class="buybtnemail btn btn-warning btn-lg btn-block" id="paypal-buy-btn"></p>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </asp:Panel>
                    </div>
                    <asp:Panel ID="paypal_order_form" runat="server">
                        <input type="hidden" name="cmd" value="_cart">
                        <input type="hidden" name="upload" value="1">
                        <input type="hidden" name="business" value="example@example.com">
                        <div id="item_1" class="itemwrap">
                            <input type="hidden" name="item_name_1" value="Paris city Tour">
                            <input type="hidden" name="item_number_1" value="323">
                            <input type="hidden" name="quantity_1" value="1">
                            <input type="hidden" name="amount_1" value="450">
                        </div>
                        <input type="hidden" name="currency_code" value="USD">

                        <input type="hidden" name="invoice" value="package_15313928069439">
                        <input type="hidden" name="notify_url" value="http://almosafertrip.net/wp-content/plugins/themeum-core/payment/wp-remote-receiver.php">
                        <input type="hidden" name="return" value="">
                        <input type="hidden" name="rm" value="2">
                        <input type="hidden" name="cancel_return" value="">
                    </asp:Panel>
                </div>
                <div class="subtitle-cover" style="background-image: url(<%# Eval("urlCover") %>); background-repeat: no-repeat; background-size: cover; background-position: 50% 50%;">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2><%# Eval("name") %></h2>
                                <ol class="breadcrumb">
                                    <li><a href="http://almosafertrip.net" class="breadcrumb_home"><asp:Literal ID="Literal3" Text="<%$Resources:General.aspx,lblHome%>" meta:resourcekey="lblHome" runat="server" /></a></li>
                                    <li class="active"><%# Eval("name") %></li>
                                </ol>

                            </div>
                            <!--//col-sm-12-->
                        </div>
                        <!--//row-->
                    </div>
                    <!--//container-->
                </div>
                <!--//moview-cover-->

                <div class="package-details-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="packagedetailin clearfix">
                                <div class="pull-right">
                                    <button id="btnBookModal" class="buynowbtn btn btn-success" data-effect="mfp-zoom-in">
                                        <asp:Label ID="lblBN" runat="server" Text="<%$Resources:tourPackage.aspx,lblBN%>" meta:resourcekey="lblBN"></asp:Label>
                                        </button>
                                </div>
                                <div class="col-sm-9">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs package-nav-tab" role="tablist">
                                        <li class="active"><a href="#tourinfo" role="tab" data-toggle="tab">
                                             <asp:Label ID="lbltourinf" runat="server" Text="<%$Resources:tourPackage.aspx,lbltourinf%>" meta:resourcekey="lbltourinf"></asp:Label> 
                                            </a></li>
                                        <li><a href="#itinerary" role="tab" data-toggle="tab">
                                            <asp:Label ID="lbltourIti" runat="server" Text="<%$Resources:tourPackage.aspx,lbltourIti%>" meta:resourcekey="lbltourIti"></asp:Label> 
                                             </a></li>
                                        <li><a href="#guide" role="tab" data-toggle="tab">
                                            <asp:Label ID="lbltourgid" runat="server" Text="<%$Resources:tourPackage.aspx,lbltourgid%>" meta:resourcekey="lbltourgid"></asp:Label> 
                                             </a></li>
                                        <li><a href="#hotelinfo" role="tab" data-toggle="tab">
                                             <asp:Label ID="lblhotinfo" runat="server" Text="<%$Resources:tourPackage.aspx,lblhotinfo%>" meta:resourcekey="lblhotinfo"></asp:Label> 
                                             </a></li>
                                        <li><a href="#tourvideo" role="tab" data-toggle="tab">
                                            <asp:Label ID="lvlvid" runat="server" Text="<%$Resources:tourPackage.aspx,lvlvid%>" meta:resourcekey="lvlvid"></asp:Label> 
                                            </a></li>
                                    </ul>
                                    <!--/.package-nav-tab-->

                                    <div class="tab-content package-tab-content">

                                        <!--tourinfo-->
                                        <div class="tab-pane fade in active" id="tourinfo">
                                            <div class="package-details-gallery">
                                                <div class="row margin-bottom photo-gallery-item">
                                                    <div class="photo-gallery-items col-sm-6 col-md-4">
                                                        <div class="gallery-items-img">
                                                            <a href="<%# Eval("urlDesc") %>" class="plus-icon">
                                                                <img src="<%# Eval("urlDesc") %>" class="img-responsive" alt="photo : "></a>
                                                        </div>
                                                        <!--/.gallery-items-img-->
                                                    </div>
                                                    <!--/.col-md-3-->
                                                </div>
                                                <!--/.row-->
                                            </div>
                                            <!--/.package-details-gallery-->
                                            <div class="package-details-content">
                                                <h3 class="title">
                                                   <asp:Label ID="lblTour" runat="server" Text="<%$Resources:tourPackage.aspx,lblTour%>" meta:resourcekey="lblTour">Tour Details</asp:Label> 
                                                    </h3>
                                                <p><%# Eval("tourDetails") %></p>

                                                <div class="package-details-choose">
                                                    <h3 class="title">
                                                        <asp:Label ID="lblwhy" runat="server" Text="<%$Resources:tourPackage.aspx,lblwhy%>" meta:resourcekey="lblwhy"></asp:Label> 
                                                                                              
                                                        </h3>
                                                    <ul class="clearfix">
                                                        <asp:ListView ID="lvChoosePack" ClientIDMode="Static" runat="server">
                                                            <ItemTemplate>
                                                                <li><span><i class="fa fa-check"></i><%# Eval("text") %></span></li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                </div>

                                                <!--/.package-details-choose-->

                                                <div class="package-details-tourmap">
<%--                                                    <iframe src="https://www.google.com/maps/place/<%# Eval("city") %>, + <%# Eval("country") %>"></iframe> --%>
<%--                                                    <iframe src="https://www.google.com/maps/embed/v1/place?q=<%# Eval("city") %>,<%# Eval("country") %>&amp;key=AIzaSyDztlrk_3CnzGHo7CFvLFqE_2bUKEq1JEU" style="width:100%;" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>--%>
                                                    <div id="map"></div>
<%--                                                    <div id="map_canvas"></div>--%>

                                                </div>
                                                <!--/.package-details-map-->

                                            </div>
                                            <!--/.package-details-content-->
                                        </div>
                                        <!--/.tab-pane-->
                                        <!--/#tourinfo-->

                                        <!--itinerary-->
                                        <div class="tab-pane fade in" id="itinerary">
                                            <div class="package-details-itinerary">
                                                <asp:ListView ID="lvTourItin" ClientIDMode="Static" runat="server">
                                                    <ItemTemplate>
                                                        <div class="media common-media-list">
                                                            <div class="pull-left">
                                                                <img class="img-responsive" src="<%# Eval("url") %>" alt="photo">
                                                            </div>
                                                            <!--/.pull-left-->

                                                            <div class="media-body">
                                                                <h3 class="title"><strong>
                                                                    <asp:Label ID="lblday" runat="server" Text="<%$Resources:tourPackage.aspx,lblday%>" meta:resourcekey="lblday"></asp:Label> 
                                                                 
                                                                     <%# Eval("dayNum") %>, </strong> <%# Eval("name") %></h3>
                                                                <div class="media-body-content">
                                                                    <%# Eval("textDesc") %>                                                                    
                                                                </div>
                                                                <!--/.media-body-content-->
                                                            </div>
                                                            <!--/.media-body-->
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                                <!--/.media-->
                                            </div>
                                            <!--/.package-details-itinerary-->
                                        </div>
                                        <!--/.tab-pane-->
                                        <!--/#itinerary-->

                                        <!--guide-->
                                        <div class="tab-pane fade in" id="guide">
                                            <asp:ListView ID="lvTourGuid" runat="server">
                                                <ItemTemplate>
                                                    <div class="package-details-guide">
                                                        <div class="media common-media-list">
                                                            <div class="pull-left" style=" width: 33.5%;">
                                                                <img class="img-responsive" style=" width: 100%; border-radius:unset;" src="<%# Eval("url") %>" alt="photo">
                                                            </div>
                                                            <!--/.pull-left-->

                                                            <div class="media-body">
                                                                <h3 class="title"><%# Eval("name") %></h3>
                                                                <div class="media-body-content">
                                                                    <%# Eval("about") %>
                                                                </div>
                                                                <!--/.media-body-content-->
                                                                <ul class="package-share">
                                                                    <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                                    <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                                    <li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                                                    <li><a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                                                </ul>

                                                            </div>
                                                            <!--/.media-body-->
                                                        </div>
                                                        <!--/.media-->
                                                        <%--                                                <div class="media common-media-list">
                                                    <div class="pull-left">
                                                        <img class="img-responsive" src="http://almosafertrip.net/wp-content/uploads/2016/08/5.png" alt="photo">
                                                    </div>
                                                    <!--/.pull-left-->

                                                    <div class="media-body">
                                                        <h3 class="title">George Miller </h3>
                                                        <div class="media-body-content">
                                                            CUSTOMIZED GUIDED TOURS, FAMILY FRIENDLY - In Paris - Lille - Reims
                                        I am a full-time qualified guide and tour manager, mainly with English-speaking visitors but also Spanish and French. For bigger groups (more than 10 people), I can help creating tours upon request all over Europe.														
                                                        </div>
                                                        <!--/.media-body-content-->
                                                        <ul class="package-share">
                                                            <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                                            <li><a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                                        </ul>

                                                    </div>
                                                    <!--/.media-body-->
                                                </div>
                                                <!--/.media-->--%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                            <!--/.package-details-guide-->
                                        </div>
                                        <!--/.tab-pane-->
                                        <!--/#guide-->

                                        <!--Hotel Info-->
                                        <asp:ListView ID="lvHotelInfo" runat="server" ClientIDMode="Static" >
                                            <ItemTemplate>
                                                <div class="tab-pane fade in" id="hotelinfo">
                                                    <div class="package-details-hotel">


                                                        <div class="media common-media-list">
                                                            <div class="pull-left">
                                                                <a href="#">
                                                                    <img class="img-responsive" src="<%# Eval("url") %>" alt="photo"></a>
                                                            </div>
                                                            <!--/.pull-left-->


                                                            <div class="media-body">
                                                                <h3 class="title"><a href="hotelinformation.aspx?ID=<%# Eval("ID")%>"><%# Eval("name")%></a></h3>
                                                                <div class="media-body-content"><%# Eval("textDesc") %></div>
                                                                <!--/.media-body-content-->

                                                                <ul class="package-share">
                                                                    <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                                    <li><a href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                                    <li><a href="https://accounts.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                                                    <li><a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                                                </ul>
                                                            </div>
                                                            <!--/.media-body-->
                                                        </div>
                                                        <!--/.media-->
                                                    </div>
                                                    <!--/.package-details-hotel-->
                                                </div>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <!--/.tab-pane-->
                                        <!--/#Hotel Info-->

                                        <!--Tour Video-->
                                        <div class="tab-pane fade in" id="tourvideo">
                                            <div class="package-details-video">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="tour-video leading">
                                                            <img class="img-responsive" src="<%# Eval("urlTourImg1") %>" alt="photo">
                                                            <a class="btn-video" href="<%# Eval("urlTourVid1") %>"><i class="fa fa-play-circle-o"></i></a>

                                                            <h3 class="title"><%# Eval("TourTitle1") %></h3>
                                                        </div>
                                                        <!--/.tour-video-->
                                                    </div>
                                                    <!--/.tour-video-->
                                                    <div class="col-sm-4">
                                                        <div class="tour-video">
                                                            <img class="img-responsive" src="<%# Eval("urlTourImg2") %>" alt="photo">
                                                            <a class="btn-video" href="<%# Eval("urlTourVid2") %>"><i class="fa fa-play-circle-o"></i></a>

                                                            <h3 class="title"><%# Eval("TourTitle2") %></h3>
                                                        </div>
                                                        <!--/.tour-video-->
                                                    </div>
                                                    <!--/.col-sm-3-->
                                                    <div class="col-sm-4">
                                                        <div class="tour-video">
                                                            <img class="img-responsive" src="<%# Eval("urlTourImg3") %>" alt="photo">
                                                            <a class="btn-video" href="<%# Eval("urlTourVid3") %>"><i class="fa fa-play-circle-o"></i></a>

                                                            <h3 class="title"><%# Eval("TourTitle3") %></h3>
                                                        </div>
                                                        <!--/.tour-video-->
                                                    </div>
                                                    <!--/.col-sm-3-->
                                                    <div class="col-sm-4">
                                                        <div class="tour-video">
                                                            <img class="img-responsive" src="<%# Eval("urlTourImg3") %>" alt="photo">
                                                            <a class="btn-video" href="<%# Eval("urlTourVid3") %>"><i class="fa fa-play-circle-o"></i></a>

                                                            <h3 class="title"><%# Eval("TourTitle3") %></h3>
                                                        </div>
                                                        <!--/.tour-video-->
                                                    </div>
                                                    <!--/.col-sm-3-->
                                                </div>
                                                <!--/.row-->
                                            </div>
                                            <!--/.package-details-video-->
                                        </div>
                                        <!--/.tab-pane-->
                                        <!--/#Tour Video-->
                                    </div>
                                    <!--/.package-tab-content-->

                                </div>
                                <!-- //col-sm-9 -->

                                <!--Sidebar-->
                                <div class="col-sm-3">
                                    <%-- mas --%>
                                    <div class="package-sidebar ">
                                        <h3 class="title">
                                            <asp:Label ID="lblAdditi" runat="server" Text="<%$Resources:tourPackage.aspx,lblAdditi%>" meta:resourcekey="lblAdditi">Additional Info</asp:Label>
                                            </h3>
                                        <ul>
                                            <li><span>
                                                <asp:Label ID="lblchkin" runat="server" Text="<%$Resources:tourPackage.aspx,lblchkin%>" meta:resourcekey="lblchkin">Check In :</asp:Label>
                                                </span><%# Eval("chckIn", "{0:d MMMM, yyyy}") %> </li>
                                            <li><span> 
                                                 <asp:Label ID="lblchkout" runat="server" Text="<%$Resources:tourPackage.aspx,lblchkout%>" meta:resourcekey="lblchkout">Check Out :</asp:Label>
                                                </span><%# Eval("chckOut", "{0:d MMMM, yyyy}") %> </li>
                                            <li><span>
                                                 <asp:Label ID="lbldur" runat="server" Text="<%$Resources:tourPackage.aspx,lbldur%>" meta:resourcekey="lbldur">Duration :</asp:Label>
                                                  </span><%# Eval("duration") %> </li>
                                            <li><span> 
                                                 <asp:Label ID="lblpers" runat="server" Text="<%$Resources:tourPackage.aspx,lblpers%>" meta:resourcekey="lblpers">Person :</asp:Label>
                                                </span><%# Eval("ppl") %> </li>
                                            <li><span> 
                                                 <asp:Label ID="lblavai" runat="server" Text="<%$Resources:tourPackage.aspx,lblavai%>" meta:resourcekey="lblavai">Availability :</asp:Label>
                                                </span><%# Eval("avail") %> </li>
                                            <li><span> 
                                                <asp:Label ID="lblprice" runat="server" Text="<%$Resources:tourPackage.aspx,lblprice%>" meta:resourcekey="lblprice">Price :</asp:Label>
                                                </span>$<%# Eval("price") %> </li>
                                            <li><span> 
                                                <asp:Label ID="lblloc" runat="server" Text="<%$Resources:tourPackage.aspx,lblloc%>" meta:resourcekey="lblloc">Location :</asp:Label>
                                                </span><%# Eval("city") %>, <%# Eval("country") %> </li>
                                        </ul>

                                        <div class="need-help">
                                            <h3 class="title">
                                                <asp:Label ID="lblhelp" runat="server" Text="<%$Resources:tourPackage.aspx,lblhelp%>" meta:resourcekey="lblhelp">Need Help!</asp:Label>
                                                </h3>
                                            <div>
                                                <asp:Label ID="lblparg" runat="server" Text="<%$Resources:tourPackage.aspx,lblparg%>" meta:resourcekey="lblparg">Our team has imagined more than 220 excursions to help you discover monuments, historical locations and activities that make Paris and France unique destinations :</asp:Label>
                                                </div>
                                            <p><i class="fa fa-phone-square"></i>
                                                <asp:Label ID="lblPhoneHelp" Text="+201000123076" runat="server" /></p>
                                            <p><i class="fa fa-envelope-square"></i><asp:Label ID="lblEmailHelp" Text="info@almosafertrip.net" runat="server" /></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.packagedetailin-->
                        </div>
                        <!--/.row-->

                        <!-- Recommend Package -->

                    </div>
                    <!--/.container-->
                </div>
                <!--/.package-details-wrap-->
            </ItemTemplate>
        </asp:ListView>
        <div class="container recTours" >
            <div class="recommend-package">
                <h3 class="title">
                    <asp:Label ID="lblrecom" runat="server" Text="<%$Resources:tourPackage.aspx,lblrecom%>" meta:resourcekey="lblrecom">Recommend Tour Packages :</asp:Label>
                    </h3>
                <div class="row">
                    <asp:ListView ID="lvPackRec" ClientIDMode="Static" runat="server">
                        <ItemTemplate>
                            <div class="col-md-4">
                                <div class="package-list-wrap ">
                                    <img width="570" height="400" src="<%# Eval("url") %>" class="img-responsive wp-post-image" alt="">
                                    <div class="package-list-content">
                                        <!-- <p class="package-list-duration"> 2 Days 3 Nights </p> -->
                                        <p class="package-list-duration"><%# Eval("textDesc") %></p>
                                        <h3 class="package-list-title"><a href="tourPackage.aspx?ID=<%# Eval("tourID") %>"><%# Eval("name") %></a></h3>
                                        <a class="package-list-button" href="tourPackage.aspx?ID=<%# Eval("tourID") %>">
                                            <asp:Label runat="server" ID="lblbook" Text="<%$Resources:tourPackage.aspx,lblbook%>" meta:resourcekey="lblbook"></asp:Label>
                                            </a>
                                    </div>
                                </div>
                                <!--/.package-list-wrap-->
                            </div>
                            <!--/.col-md-3-->
                        </ItemTemplate>
                    </asp:ListView> <!-- //row -->
                </div>
            </div> <!-- //recommend-package -->
        </div>
        <div class="clearfix"></div>
    </section>


    <%--Start Alert--%>
    <asp:Panel ID="myAlert" runat="server">
        <h2 class="text-center"><a href="default.aspx"><asp:Literal ID="Literal3" Text="<%$Resources:General.aspx,lblHomePg%>" meta:resourcekey="lblHomePg" runat="server" /></a></h2>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> <asp:Label ID="lblAlert"  runat="server" Text="<%$Resources:Default.aspx,lblAlert%>" meta:resourcekey="lblAlert" />!</strong>
        </div>
        <div class="modalBG alert-dismissible" style="
            min-height: 1602px;
            position: fixed;
            width: 100%;
            top: 0;
            left: 0;
            background: rgba(0,0,0,0.8);
            z-index: -1;
        "></div>
    </asp:Panel>
    <%--End Alert--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
        <script>
            $("[href='#guide']").click(function () {

                if ($(".tab-pane").hasClass('active')) {
                    $(".tab-pane").removeClass('active');
                }
                if ($(".tab-pane").hasClass('show')) {
                    $(".tab-pane").removeClass('show');
                }
            $("#guide").addClass('active show');
        });
    </script>
<%--    <script src="https://maps.googleapis.com/maps/api/js"></script>--%>

<%--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDztlrk_3CnzGHo7CFvLFqE_2bUKEq1JEU&callback=initMap" async defer></script>--%>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3Fdde_DRilu_UaQWnxnsPf3xkioxOBNg&callback=initMap" async defer></script>
<%--    <script>
        $(document).ready(function () {
            var map;
            function initMap() {
                var geocoder = new google.maps.Geocoder();

                map = new google.maps.Map(document.getElementById('map'), {
                    center: { lat: 0, lng: 0 },
                    zoom: 8
                });
                var cityName = "Paris";
                geocoder.geocode({ 'address': cityName }, function (results, status) {
                    if (status === 'OK') {
                        map.setCenter(results[0].geometry.location);
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });


            }
        });
    </script>--%>
<%--    <script>
        $(document).ready(function initialize() {
            var address = 'Zurich, Ch';

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var Lat = results[0].geometry.location.lat();
                    var Lng = results[0].geometry.location.lng();
                    var myOptions = {
                        zoom: 11,
                        center: new google.maps.LatLng(Lat, Lng)
                    };
                    var map = new google.maps.Map(
                      document.getElementById("map_canvas"), myOptions);
                } else {
                    alert("Something got wrong " + status);
                }
            });
        });
        google.maps.event.addDomListener(window, "load", initialize);
    </script>--%>
    <script> google.maps.event.addDomListener(window, 'load', init);
var map;

function init() {
    var mapOptions = {
        center: new google.maps.LatLng(30,-90),
        zoom: 10,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT,
        },
        disableDoubleClickZoom: true,
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        },
        scaleControl: true,
        scrollwheel: true,
        streetViewControl: true,
        draggable : true,
        overviewMapControl: true,
        overviewMapControlOptions: {
            opened: true,
        },
    }

    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var locations = [];

    }


</script>
    <script>
        $("#btnBookModal").click(function () {
            $(".modalPopup").attr("style", "display:block !important; z-index:3333;")
            return false;
        });
    </script>
</asp:Content>
