﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="App.aspx.cs" Inherits="almosaferwp.App" Debug="true" EnableEventValidation="false" EnableViewState="true" ValidateRequest="false" %>

<%@ Register Assembly="Evolutility.UIServer" Namespace="Evolutility" TagPrefix="EVOL" %>

<asp:Content ID="Content5" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal Text="<%$Resources:General.aspx,lblAdminTitle%>" meta:resourcekey="lblAdminTitle" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="CPHead" runat="server">
    <link href="Evol.css" rel="stylesheet" />
    <script type="text/javascript" src="scripts/jquery-1.3.2.min.js"></script>
    <style>
        ul {
            list-style-type: none;
        }

        .bottom.solid {
            display: none;
        }

        section.container-fluid {
            margin-bottom: 5%;
            background-color: white;
        }
    </style>


</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="CPContent" runat="server">
    <div class="subtitle-cover sub-title " style="background-image: url(../wp-content/uploads/2016/10/rmbackground.jpg); background-size: cover; background-position: 50% 50%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="page-leading">
                        <asp:Literal ID="Literal1" Text="<%$Resources:General.aspx,lblAdminTitle%>" meta:resourcekey="lblAdminTitle" runat="server" /></h2>
                    <ol class="breadcrumb">
                        <li><a href="default.aspx" class="breadcrumb_home">
                            <asp:Literal ID="Literal3" Text="<%$Resources:General.aspx,lblHome%>" meta:resourcekey="lblHome" runat="server" /></a></li>
                        <li class="active">
                            <asp:Literal ID="Literal2" Text="<%$Resources:General.aspx,lblAdminTitle%>" meta:resourcekey="lblAdminTitle" runat="server" /></li>
                    </ol>

                </div>
            </div>
        </div>
    </div>
    <!--/.sub-title-->
    <section class="container-fluid">
        <div class="col-md-3">
            <ul class="topnav menu-left-nest">
                <li>
                    <a href="#" style="border-left: 0px solid !important;" class="title-menu-left">
                        <span class="widget-menu"></span>
                        <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>
                    </a>
                </li>
                <asp:Repeater ID="Perm_Pages_Repeater" runat="server">
                    <ItemTemplate>
                        <li>
                            <a class="tooltip-tip ajax-load" href="App.aspx?XID=<%#Eval("XML_ID")%>&PID=<%#Eval("ID")%>" title="">
                                <i class="icon-feed"></i>
                                <span><%#Eval("NAME")%></span>
                            </a>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="content-wrap">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="nest" id="inlineClose">
                            <div class="title-alt">
                                <h6></h6>
                                <div class="titleClose">
                                </div>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#inline">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="inline">



                                <EVOL:UIServer ID="Evo1" runat="server" BackColor="#EDEDED" BackColorRowMouseOver="Beige"
                                    CssClass="main1" DBAllowDesign="true" DBAllowExport="True" DisplayModeStart="List" ShowTitle="true"
                                    Language="EN" NavigationLinks="true" RowsPerPage="20"
                                    VirtualPathDesigner="EvoDico/" SecurityKey="EvoDico"
                                    ToolbarPosition="top" VirtualPathPictures="uploads/" VirtualPathToolbar="PixEvo"
                                    Width="100%" DBAllowLogout="False" DBAllowCharts="false" />
                                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>



                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="CPScript" runat="server">
  
</asp:Content>

