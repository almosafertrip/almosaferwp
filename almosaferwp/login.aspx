﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="almosaferwp.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="assets/theme/css/login.css" rel="stylesheet" />
    <!------ Include the above in your HEAD tag ---------->
</head>
<body id="LoginForm">
    <div class="container">
<%--        <h1 class="form-heading">login Form</h1>--%>
        <div class="login-form col-md-8 col-centered col-xs-12">
            <div class="main-div">
                <div class="panel">
                    <h2>Admin Login</h2>
                    <p>Please enter your user name and password</p>
                </div>
                <form id="Login" runat="server" >
                    <div class="form-group">
                        <asp:TextBox CssClass="form-control" placeholder="user name" ID="txtUser" runat="server" />
                    </div>
                    <div class="form-group">
                        <asp:TextBox CssClass="form-control" TextMode="Password" placeholder="Password" ID="txtPass" runat="server" />
                    </div>
                    <div class="forgot text-center">
                        <a href="Default.aspx">Back to Home</a>
                    </div>
                    <asp:Button Text="Login" CssClass="btn btn-primary" ID="btnLogin" OnClick="btnLogin_Click" runat="server" />
                </form>
            </div>
            <p class="botto-text">almosafertrip &copy2018 all rights Reserved</p>
        </div>
    </div>
</body>
</html>

