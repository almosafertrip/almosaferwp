﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="about.aspx.cs" Inherits="AlmosaferWP.about" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal ID="lblTitle" Text="<%$Resources:General.aspx,lblTitleAbout%>" meta:resourcekey="lblTitleAbout" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <style>
        /*#main p span, #main h2 span {
            color: #777 !important;
        }*/
        #main {
        background-color: white !important;
        }
        .vc_column-inner.vc_custom_1475745787586 {
            background-image: url(covers/mission.jpg);
        }
        div.wpb_column.vc_column_container.vc_col-sm-3  div div div div img {
            height:255px !important;
        }
        ul {
            list-style-type:none;
            padding-left: unset;
        }
        /*fake*/
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <section id="main">
<%--        <div class="subtitle-cover sub-title " style="background-image:url(../wp-content/uploads/2016/07/blogbg.jpg);background-size: cover;background-position: 50% 50%;">--%>
          <div class="subtitle-cover sub-title " style="background-image:url(../wp-content/uploads/2016/07/blogbg.jpg);background-size: cover;background-position: 50% 50%;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="page-leading"><asp:Literal ID="litrabout" runat="server" /><%# Eval("about") %></h2>
                        <ol class="breadcrumb">
                            <li><a href="Default.aspx" class="breadcrumb_home"><asp:Literal ID="Literal2" Text="<%$Resources:General.aspx,lblHome%>" meta:resourcekey="lblHome" runat="server" /></a></li>
                            <li class="active"><asp:Literal ID="litrabout1"  runat="server" /><%# Eval("about") %></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div><!--/.sub-title-->
       
        <div class="container">
            <div id="content" class="site-content" role="main">
                <div id="post-595" class="post-595 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1475737154730">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="addon-themeum-title " style="text-align:center;">
                                            <div class="themeum-titlelayout1">
                                                <h2 class="thm-titlestandardstyle" >
                                                    <asp:Label ID="lblwho" runat="server" >
                                                        <%# Eval("whoWe") %>
                                                    </asp:Label>
                                                   
                                                </h2>
                                                <p class="thm-sub-titlestandardstylesub" >
                                                    

                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1475736156434">
                            <div class="travelkit-mission wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
                                <div class="vc_column-inner vc_custom_1475745787586">
                                    <div class="wpb_wrapper">
	                                    <div class="wpb_text_column wpb_content_element  about-details" >
		                                    <div class="wpb_wrapper">
			                                    <h2><span style="color: #ffffff;">
                                                    <asp:Label ID="lblour" runat="server" >
                                                         <%# Eval("ourMission") %>
                                                    </asp:Label>
			                                        </span></h2>
                                                <p><span style="color: #ffffff;">
                                                     <asp:Label ID="lblparg1" runat="server" >
                                                         <%# Eval("pargraph1") %>
                                                     </asp:Label>
                                                    </span></p>
                                                <p><span style="color: #ffffff;">
                                                     <asp:Label ID="lblparg2" runat="server" >
                                                           <%# Eval("pargraph2") %>
                                                     </asp:Label>
                                                    </span></p>

		                                    </div>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="themeum-photo-gallery-item ">
                                            <div class="col-xs-12 col-sm-6 col-md-6 no-padding">
                                                <div class="conference-img">
                                                    <div class="photo " style="">
                                                        <a href="<%# Eval("urlimg2") %>" class="plus-icon">
                                                            <img id="lblurlimg2" src="any"  class="img-responsive" alt="image"  runat="server" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid vc_custom_1475737541419">
                            <div class="travelket-bar wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1475742069809">
                                            <div class="wpb_column vc_column_container vc_col-sm-2">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-8">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
                                                        <div class="addon-themeum-title " style="text-align:center;">
                                                            <div class="themeum-titlelayout2">
                                                                <h2 class="thm-titlestandardstyle" >
                                                                    <br />
                                                                    <asp:Label ID="lblsimp" runat="server" Text="<%$Resources:General.aspx,lblsimp%>" meta:resourcekey="lblsimp"></asp:Label>
                                                                    

                                                                </h2>
                                                                <p class="thm-sub-titlestandardstylesub" >
                                                                   </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-2">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper"></div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1475744949087">
                                                    <asp:ListView ID="lvwork" ClientIDMode="Static" runat="server">
                                            <ItemTemplate>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class="themeum-person ">
                                                                    <div class="themeum-person-image">
                                                                        <img src="<%# Eval("urlImg") %>" class="img-responsive" alt="photo">
                                                                    </div>
                                                                    <div class="person-details">
                                                                        <h3 class="person-title">
                                                                            <asp:Label ID="lblname" runat="server">
                                                                        <%# Eval("name") %>
                                                                            </asp:Label>
                                                                        </h3>
                                                                        <h4 class="person-deg">
                                                                            <asp:Label ID="lblpostion" runat="server">
                                                                         <%# Eval("postion") %>
                                                                            </asp:Label>
                                                                        </h4>
                                                                        <p class="person-description">
                                                                            <asp:Label ID="lblpargra" runat="server">
                                                                          <%# Eval("pargraph") %>
                                                                            </asp:Label>
                                                                        </p>
                                                                    </div>
                                                                    <div class="social-icon">
                                                                        <ul class="list-unstyled list-inline">
                                                                            <li>
                                                                                <a class="facebook" href="#" target="_blank">
                                                                                    <i class="fa fa-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li><a class="twitter" href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                                            <li><a class="g-plus" href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                                                            <li><a class="linkedin" href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                            </ItemTemplate>
                                        </asp:ListView>
                                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid textwidget-area vc_custom_1475746679822">
                            <div class="textwidget wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-4">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element " >
		                                                    <div class="wpb_wrapper">
			                                                    <h2>
                                                                    <asp:Label ID="lblweDoit2" runat="server" ><%# Eval("weDoit2") %></asp:Label>
                                                                    </h2>
                                                                <ul>
                                                                    <asp:ListView ID="lvHWDI" ClientIDMode="Static" runat="server">
                                                                        <ItemTemplate>
                                                                            <li>
                                                                                <asp:Label ID="lblpargraph5" runat="server"><%# Eval ("text") %></asp:Label>

                                                                            </li>
                                                                        </ItemTemplate>
                                                                    </asp:ListView>
                                                                </ul>
                                                            </div>
	                                                    </div>
                                                    </div>
                                                </div>
                                                <asp:ListView runat="server">
                                                    <ItemTemplate></ItemTemplate>
                                                </asp:ListView>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-4">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element " >
		                                                    <div class="wpb_wrapper">
			                                                    <h4>
                                                                    <asp:Label ID="lblweDoit" runat="server" ><%# Eval("weDoit") %></asp:Label>
                                                                    </h4>
                                                               
                                                                    <p>
                                                                         <asp:Label ID="lblpargraph4" runat="server" ><%# Eval("pargraph4") %></asp:Label>
                                                                        </p>
                                                            </div>
	                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_column vc_column_container vc_col-sm-4">
                                                <div class="vc_column-inner ">
                                                    <div class="wpb_wrapper">
	                                                    <div class="wpb_text_column wpb_content_element " >
		                                                    <div class="wpb_wrapper">
			                                                    <h4>
                                                                    <asp:Label ID="lblimportant" runat="server" > <%# Eval ("important") %></asp:Label>
                                                                    </h4>
                                                                <p>
                                                                    <asp:Label ID="lblpargraph3" runat="server" ><%# Eval("pargraph3") %></asp:Label>
                                                                    </p>

		                                                    </div>
	                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--/#content-->
        </div>
    </section> <!--/#main-->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
