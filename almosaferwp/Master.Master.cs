﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using almosaferwp;

namespace AlmosaferWP
{
    public partial class Master : System.Web.UI.MasterPage
    {


        basePage basePage = new basePage();
        CodeClass codeClass = new CodeClass();

        public string tblContactUSAr { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loadFooter();
                loadContactUs();
            }
            
        }


        protected void btnLangEn_Click(object sender, EventArgs e)
        {
            Session["SelectedLanguage"] = "English";
            //basePage.InitializeCulture();

            Server.Transfer(Request.Url.PathAndQuery);
        }
        //protected void InitializeCultureAr()
        //{
        //    // Set selectedCulture appropriately.
        //    // You can get it from QueryString or ...
        //    string selectedCultue = "ar-EG";
        //    CultureInfo userCulture = CultureInfo.CreateSpecificCulture(selectedCultue);
        //    this.Attributes.Add("Culture", selectedCultue);
        //    this.Attributes.Add("UICulture", "auto");
        //    this.Page.Culture = "Arabic (Egypt)";
        //    this.Page.UICulture = "auto";
        //    // You can set the culture of the current page.
        //    //UICulture = selectedCultue;
        //    //Culture = selectedCultue;


        //    // and/or the culture of the current thread
        //    System.Threading.Thread.CurrentThread.CurrentCulture = userCulture;
        //    System.Threading.Thread.CurrentThread.CurrentUICulture = userCulture;

        //    //Master.Attributes.Add("Culture", selectedCultue);



        //}
        //protected void InitializeCultureEn()
        //{
        //    // Set selectedCulture appropriately.
        //    // You can get it from QueryString or ...
        //    string selectedCultue = "en";
        //    CultureInfo userCulture = new CultureInfo(selectedCultue);


        //    // You can set the culture of the current page.
        //    //UICulture = selectedCultue;
        //    //Culture = selectedCultue;


        //    // and/or the culture of the current thread
        //    System.Threading.Thread.CurrentThread.CurrentCulture = userCulture;
        //    System.Threading.Thread.CurrentThread.CurrentUICulture = userCulture;



        //}

        protected void loadFooter()
        {

            string tblFixedData = "fixedData";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblFixedData = "fixedDataAr";
            }
            string[] country = new string[] { "Egypt", "Saudi", "UAE", "USA" };
            for (int i = 0; i < 4; i++)
            {
                // get id string for controls

                string lblLocatID = "lblLocat" + country[i];
                string lblCountryID = "lblCountry" + country[i];
                string lblAddressID = "lblAddress" + country[i];
                string lblPhoneID = "lblPhone" + country[i];
                string lblEmailID = "lblEmail" + country[i];

                // find controls

                Label lblLocat = (Label)this.FindControl(lblLocatID);
                Label lblCountry = (Label)this.FindControl(lblCountryID);
                Label lblAddress = (Label)this.FindControl(lblAddressID);
                Label lblPhone = (Label)this.FindControl(lblPhoneID);
                Label lblEmail = (Label)this.FindControl(lblEmailID);

                DataSet dsFixedData = codeClass.Sqlread("select * from " + tblFixedData + " where countryIndex = '" + country[i] + "'");
                lblLocat.Text = dsFixedData.Tables[0].Rows[0]["location"].ToString();
                lblCountry.Text = dsFixedData.Tables[0].Rows[0]["country"].ToString();
                lblAddress.Text = dsFixedData.Tables[0].Rows[0]["address"].ToString();
                lblPhone.Text = dsFixedData.Tables[0].Rows[0]["phone"].ToString();
                lblEmail.Text = dsFixedData.Tables[0].Rows[0]["email"].ToString();

            }


        }
        protected void btnLangAr_Click(object sender, EventArgs e)
        {
            Session["SelectedLanguage"] = "Arabic";
            //basePage.InitializeCulture();

            Server.Transfer(Request.Url.PathAndQuery);

        }
        protected void loadContactUs()
        {
            string tblContactUS = "ContactUS";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblContactUS = "ContactUSAr";
            }
            
            DataSet dsContactUs = codeClass.Sqlread("select top(1) * from "+tblContactUS);
            lblCallUs.Text = dsContactUs.Tables[0].Rows[0]["callUs"].ToString();
            lblnumber.Text = dsContactUs.Tables[0].Rows[0]["number"].ToString();
            lblEmail.Text = dsContactUs.Tables[0].Rows[0]["Email"].ToString();
            lblInF.Text = dsContactUs.Tables[0].Rows[0]["EmailType"].ToString();
          
        }
    }
}
