﻿using almosaferwp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AlmosaferWP
{
    public partial class package_list : basePage
    {
        CodeClass codeClass = new CodeClass();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    loadPackages();
                    myAlert.Visible = false;
                }
            }
            catch
            {
                myAlert.Visible = true;
            }
            
        }
        protected void loadPackages()
        {
            string tblName = "Pakageelmosafer";
            string sess = Session["SelectedLanguage"].ToString();
            if ( sess == "Arabic")
            {
                tblName = "PakageelmosaferAr";
            }
            DataSet dsPack = codeClass.Sqlread("select * from " + tblName);
            lvPackages.DataSource = dsPack;
            lvPackages.DataBind();
        }

    }
}