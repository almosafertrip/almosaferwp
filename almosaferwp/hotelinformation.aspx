﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="hotelinformation.aspx.cs" Inherits="almosaferwp.hotelinformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPTitle" runat="server">
    <asp:Literal Text="Hotel Name" ID="lblHotelName" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHead" runat="server">
    <link href="wp-content/themes/travelkit/css/maindc98.css" rel="stylesheet" />
    <style>
        .owl-item {
            display: none;
        }

        .owl-item.active {
            display: block;
            touch-action: cross-slide-x;
        }
        .package-list-wrap {
            height:223.85px;
        }
        /* alert style */
        #CPContent_myAlert {
        
            width: 80%;
            margin: auto;
            position: fixed;
            left: 10.5%;
            top: 20%;
            height: auto;
            word-break: break-word;
            z-index: 4444;

        }
        #CPContent_myAlert div {
            
            height: inherit;
            font-size: large;
            text-align: center;
            border: black 1px solid;

        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CPContent" runat="server">
    <asp:ListView ID="lvHotelInfo" ClientIDMode="Static" OnItemDataBound="lvHotelInfo_ItemDataBound" runat="server">
        <ItemTemplate>
            <section id="main" class="package-single clearfix" style="background-color: white">

                <!-- buy now button popup -->

                <div class="subtitle-cover" style="background-image: url(<%# Eval("url") %>); background-repeat: no-repeat; background-size: cover; background-position: 50% 50%;">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2><%# Eval("name") %></h2>
                                <ol class="breadcrumb">
                                    <li><a href="Default.aspx" class="breadcrumb_home"><asp:Literal ID="Literal3" Text="<%$Resources:General.aspx,lblHome%>" meta:resourcekey="lblHome" runat="server" /></a></li>
                                    <li class="active"><%# Eval("name") %></li>
                                </ol>

                            </div>
                            <!--//col-sm-12-->
                        </div>
                        <!--//row-->
                    </div>
                    <!--//container-->
                </div>
                <!--//moview-cover-->

                <div class="package-details-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="packagedetailin clearfix">
                                <div class="col-sm-9">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs package-nav-tab" role="tablist">
                                        <li class="active"><a href="#hoteldetail" role="tab" data-toggle="tab">
                                            <asp:Label ID="lbldesc" runat="server" Text="<%$Resources:General.aspx,lbldesc%>" meta:resourcekey="lbldesc"></asp:Label>
                                            </a></li>

                                        <li><a href="#roomtype" role="tab" data-toggle="tab">
                                            <asp:Label ID="lblromTy" runat="server" Text="<%$Resources:General.aspx,lblromTy%>" meta:resourcekey="lblromTy"></asp:Label>
                                            </a></li>

                                       
                                        <li><a href="#hotelvideo" role="tab" data-toggle="tab">
                                             <asp:Label ID="lblvedi" runat="server" Text="<%$Resources:General.aspx,lblvedi%>" meta:resourcekey="lblvedi"></asp:Label>
                                            </a></li>
                                    </ul>
                                    <!--/.package-nav-tab-->

                                    <div class="tab-content package-tab-content">

                                        <!--hoteldetail-->
                                        <div class="tab-pane fade in active" id="hoteldetail">
                                            <h3 class="title"><%# Eval("name") %></h3>
                                            <p><%# Eval("hotelDesc") %></p>


                                            <div class="package-details-gallery hotel-details-gallery">
                                                <div class="row margin-bottom photo-gallery-item">

                                                    <div class="photo-gallery-items col-sm-6 col-md-4">
                                                        <div class="gallery-items-img">
                                                            <a href="<%# Eval("urlDesc") %>" class="plus-icon">
                                                                <img src="<%# Eval("urlDesc") %>" class="img-responsive" alt="photo : "></a>
                                                        </div>
                                                        <!--/.gallery-items-img-->
                                                    </div>
                                                    <!--/.col-md-3-->
                                                </div>
                                                <!--/.row-->
                                            </div>
                                            <!--/.package-details-gallery-->
                                            <div class="package-details-content">

                                                <div class="package-details-choose">
                                                    <h3 class="title">
                                                         <asp:Label ID="lblfaci" runat="server" Text="<%$Resources:General.aspx,lblfaci%>" meta:resourcekey="lblfaci"></asp:Label>
                                                    </h3>
                                                    <ul class="clearfix">
                                                        <asp:ListView ID="lvHtlFacilities" ClientIDMode="Static" runat="server">
                                                            <ItemTemplate>
                                                                <li><span><i class="fa fa-check"></i><%# Eval("text") %> </span></li>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </ul>
                                                </div>
                                                <!--/.package-details-choose-->


                                            </div>
                                            <!--/.package-details-content-->
                                        </div>
                                        <!--/.tab-pane-->
                                        <!--/#hoteldetail-->

                                        <!--room info-->
                                        <div class="tab-pane fade in" id="roomtype">
                                            <div class="hotel-details-roomtype">
                                                <div class="row">
                                                    <asp:ListView ID="lvRoom" ClientIDMode="Static" runat="server">
                                                        <ItemTemplate>
                                                            <div class="col-sm-6" style="max-height: 600px !important; margin-bottom: 20px;">



                                                                <div class="package-details-gallery">

                                                                    <div class="owl-stage-outer">
                                                                        <div class="owl-stage">
                                                                            <div class="owl-item active" style="margin-left: 0px;">
                                                                                <div class="item">
                                                                                    <a href="<%# Eval("url") %>" class="plus-icon">
                                                                                        <img src="<%# Eval("url") %>" class="img-responsive" alt="photo : "></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/.room-gallery-->
                                                                </div>
                                                                <!--/.package-details-gallery-->
                                                            </div>
                                                            <!-- /.col-sm-6 -->
                                                            <div class="col-sm-6">
                                                                <div class="hotel-info">
                                                                    <span class="price"><%# Eval("price") %> </span>
                                                                    <p class="room-duration">
                                                                        <asp:Label ID="lblperN" runat="server" Text="<%$Resources:General.aspx,lblperN%>" meta:resourcekey="lblperN"></asp:Label>
                                                                        </p>
                                                                    <p class="room-type"><%# Eval("name") %></p>
                                                                    <p class="room-info"><%# Eval("textDesc") %></p>
                                                                </div>
                                                                <!-- //.hotel-info -->
                                                            </div>
                                                            <!-- /.col-sm-6 -->
                                                            <div class="clearfix mtb30"></div>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                            <!--/.package-details-itinerary-->
                                        </div>
                                        <!--/.tab-pane-->
                                        <!--/#roominfo-->


                                        <!--Tour Video-->
                                        <div class="tab-pane fade in" id="hotelvideo">
                                            <div class="package-details-video">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="tour-video leading">
                                                            <img src="<%# Eval("urlvidimg") %>" />
                                                            <a href="<%# Eval("urlvidimg") %>" class="plus-icon">
                                                                <%--<img class="img-responsive" src="wp-content/uploads/2016/07/848221.jpg" alt="photo">--%>
                                                            </a><a class="btn-video" href="<%# Eval("urlvid") %>"><i class="fa fa-play-circle-o"></i></a>

                                                            <h3 class="title"><%# Eval("vidTitle") %> </h3>
                                                        </div>
                                                        <!--/.tour-video-->
                                                    </div>
                                                    <!--/.tour-video-->
                                                </div>
                                                <!--/.row-->
                                            </div>
                                            <!--/.package-details-video-->
                                        </div>
                                        <!--/.tab-pane-->
                                        <!--/#Tour Video-->
                                    </div>
                                    <!--/.package-tab-content-->

                                </div>
                                <!-- //col-sm-9 -->

                                <!--Sidebar-->
                                <div class="col-sm-3">
                                    <div class="package-sidebar  bigtop">
                                        <h3 class="title">
                                            <asp:Label ID="lbladdi" runat="server" Text="<%$Resources:General.aspx,lbladdi%>" meta:resourcekey="lbladdi"></asp:Label>
                                           </h3>
                                        <ul>
                                            <li><span>
                                                <asp:Label ID="lblchkin1" runat="server" Text="<%$Resources:General.aspx,lblchkin1%>" meta:resourcekey="lblchkin1"></asp:Label>
                                                </span><%# Eval("chkIn") %> </li>
                                            <li><span>
                                                 <asp:Label ID="lblchkout1" runat="server" Text="<%$Resources:General.aspx,lblchkout1%>" meta:resourcekey="lblchkout1"></asp:Label>
                                                </span><%# Eval("chkOut") %> </li>
                                            <li><span> 
                                                <asp:Label ID="lblpriceing" runat="server" Text="<%$Resources:General.aspx,lblpriceing%>" meta:resourcekey="lblpriceing"></asp:Label>
                                                </span><%# Eval("Pricing") %> </li>
                                            <li><span> 
                                                <asp:Label ID="lbltoroom" runat="server" Text="<%$Resources:General.aspx,lbltoroom%>" meta:resourcekey="lbltoroom"></asp:Label>
                                                </span><%# Eval("TotalRoom") %> </li>
                                            <li><span> 
                                                <asp:Label ID="lblloc" runat="server" Text="<%$Resources:General.aspx,lblloc%>" meta:resourcekey="lblloc">Location :</asp:Label>
                                                </span><%# Eval("Location") %> </li>
                                        </ul>

                                        <div class="need-help">
                                            <h3 class="title">
                                                 <asp:Label ID="lblhelp" runat="server" Text="<%$Resources:General.aspx,lblloc%>" meta:resourcekey="lblhelp"></asp:Label>
                                                </h3>
                                            <div>
                                                <asp:Label ID="lblpr" runat="server" Text="<%$Resources:General.aspx,lblpr%>" meta:resourcekey="lblpr"></asp:Label>
                                            <p><i class="fa fa-phone-square"></i>207-555-5555</p>
                                            <p><i class="fa fa-envelope-square"></i>travelkit@themeum.com </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.packagedetailin-->
                        </div>
                    <!--/.row-->

                    <!-- Recommend Package -->
                    <div class="recommend-package">

                        <h3 class="title">
                            <asp:Label ID="lblreco" runat="server" Text="<%$Resources:General.aspx,lblreco%>" meta:resourcekey="lblreco"></asp:Label>
                            </h3>
                        <div class="row margin-bottom">

                            <asp:ListView ID="lvRelatedHotels" ClientIDMode="Static" runat="server">
                                <ItemTemplate>
                                    <div class="col-md-4">
                                        <div class="package-list-wrap">
                                            <img width="570" height="400" src="<%# Eval("url") %>" class="img-responsive wp-post-image" alt="">
                                            <div class="package-list-content">

                                                <p class="package-list-duration">
                                                     <asp:Label ID="lblstrt" runat="server" Text="<%$Resources:General.aspx,lblstrt%>" meta:resourcekey="lblstrt"></asp:Label>
                                                    <%# Eval("Pricing") %></p>
                                                <h3 class="package-list-title"><a href="hotelinformation.aspx?ID=<%# Eval("ID") %>"><%# Eval("name") %></a></h3>
                                                <a class="package-list-button" href="hotelinformation.aspx?ID=<%# Eval("ID") %>">
                                                    <asp:Label ID="lblbooknw" runat="server" Text="<%$Resources:General.aspx,lblbooknw%>" meta:resourcekey="lblbooknw"></asp:Label>
                                                    </a>
                                            </div>
                                        </div>
                                        <!--/.package-list-wrap-->
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                            <!--/.col-md-3-->

                        </div>
                        <!-- //row -->
                    </div>
                    <!-- //recommend-package -->
                    </div>
                    <!--/.container-->
                </div>
                <!--/.package-details-wrap-->
            </section>
        </ItemTemplate>
    </asp:ListView>
    
    <%--Start Alert--%>
    <asp:Panel ID="myAlert" runat="server">
        <h2 class="text-center"><a href="default.aspx"><asp:Literal ID="Literal3" Text="<%$Resources:General.aspx,lblHomePg%>" meta:resourcekey="lblHomePg" runat="server" /></a></h2>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong> <asp:Label ID="lblAlert"  runat="server" Text="<%$Resources:Default.aspx,lblAlert%>" meta:resourcekey="lblAlert" />!</strong>
        </div>
        <div class="modalBG alert-dismissible" style="
            min-height: 1602px;
            position: fixed;
            width: 100%;
            top: 0;
            left: 0;
            background: rgba(0,0,0,0.8);
            z-index: -1;
        "></div>
    </asp:Panel>
    <%--End Alert--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CPScript" runat="server">
</asp:Content>
