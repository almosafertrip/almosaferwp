﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using almosaferwp;

namespace AlmosaferWP
{
    public partial class Default : basePage
    {
        CodeClass codeClass = new CodeClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            try
            {
                InitializeCulture();

                if (!IsPostBack)
                {
                    loadSlider();
                    loadPackages();
                    myAlert.Visible = false;
                }
            }
            catch
            {
                myAlert.Visible = true;
            }
            
        }




        //protected override void InitializeCulture()

        //{
        //    if (Session["lang"] != null)
        //    {
        //        Culture = Session["lang"].ToString();
        //        base.InitializeCulture();
        //        UICulture = Session["lang"].ToString();
        //        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Session["long"].ToString());
        //        Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(Session["long"].ToString());
        //        Session["lang"] = lblTrial.ToString();
        //        base.InitializeCulture();
        //    }
        //    else
        //    {
        //        base.InitializeCulture();
        //        Session["lang"] = lblTrial.Text.ToString();
        //        Response.Redirect("Default.aspx");
        //    }


        //}
        protected void loadSlider()
        {
            string tblName = "SliderMosafer";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "SliderMosaferAr";

            }
            DataSet ds = codeClass.Sqlread("select * from "+ tblName);
            lvSlider.DataSource = ds;
            lvSlider.DataBind();
        }
        protected void loadPackages()
        {
            string tblName = "Pakageelmosafer";
            string sess = Session["SelectedLanguage"].ToString();
            if (sess == "Arabic")
            {
                tblName = "PakageelmosaferAr";

            }
            DataSet dsPack = codeClass.Sqlread("select top(6) * from " + tblName);
            lvPackages.DataSource = dsPack;
            lvPackages.DataBind();
        }

    }
    
}